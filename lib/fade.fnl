(var fade [:in 0])

(local w (* 11 16))
(local h (* 8 16))

(local wipecanvas (love.graphics.newCanvas w h))

(var colour [1 1 1 1])

(fn set-colour [c]
  (set colour c))

(fn draw-fade [r]
  (love.graphics.setCanvas wipecanvas)
  (love.graphics.clear)
  (love.graphics.push)
  (love.graphics.setColor colour)
  (love.graphics.rectangle :fill 0 0 w h)
  (love.graphics.setBlendMode :replace)
  (love.graphics.setColor 1 1 1 0)
  (love.graphics.circle :fill (/ w 2) (/ h 2) r)
  (love.graphics.setBlendMode :alpha)
  (love.graphics.pop)
  (love.graphics.setCanvas))

(var out-callback? nil)
(var in-callback? nil)
(fn fade-out [callback]
  (set out-callback? callback)
  (set fade [:out (/ w 2)]))

(fn fade-in [callback]
  (set in-callback? callback)
  (set fade [:in 0]))


(fn update [dt]
    (match fade
    [:in x]
    (do
      (tset fade 2 (+ (* 200 dt) (. fade 2)))
      (draw-fade x)
      (if (> x 64)
          (set fade [:null 64])
          (when in-callback?
            (in-callback?))))
    [:out x]
    (do
      (tset fade 2 (- (. fade 2) (* 200 dt)))
      (draw-fade x)
      (when (< x 0)
        (set fade [:null 0])
        (when out-callback?
            (out-callback?))))
    [:null y] (do
                (love.graphics.setCanvas wipecanvas)
                (love.graphics.clear)
                (love.graphics.setCanvas))))

{: update  :out fade-out :in fade-in :canvas wipecanvas : set-colour }
