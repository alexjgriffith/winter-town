;; get batchsprite rendering implemented <done>
;; setup an example with bump
;; change delete order for objects <done>
;; y-sorting objects <done>
;; non square tiles <done> <needs work>

(local level (require :editor.level))
(local {: editor} (require :editor.interface))

(import-macros {: db} :test-suite)
(love.graphics.setDefaultFilter :nearest :nearest)

(fn love.load []
  (love.graphics.setBackgroundColor 1 1 1 1)
  )

(local atlas :editor/assets/atlas.png)

(local player {:serialize (fn [self]  (pp :encoding-player) self.encode)
               :draw (fn [self image ...] (love.graphics.draw image self.quad
                                                              self.x self.y))
               :update (fn [self dt]
                         (tset self :y (+ self.y
                                        (if (love.keyboard.isDown :down) 1 0)
                                        (if (love.keyboard.isDown :up) -1 0)))
                         (tset self :x (+ self.x (if (love.keyboard.isDown :left) -1 0)
                                          (if (love.keyboard.isDown :right) 1 0)))
                         )
               :handlers {}})

(local player-mt {:__index player})

(fn create-player [brush]
  (local {: px : py : pw : ph : iw : ih} brush)
  (setmetatable {:encode brush
                 :name :player
                 :quad (love.graphics.newQuad px py pw ph iw ih)
                 :x brush.x :y brush.y
                 :w brush.w :h brush.h}
                player-mt))

(local obj {:serialize (fn [self]  (pp :encoding-obj) self.encode)
               :draw (fn [self image ...] (love.graphics.draw image self.quad
                                                              self.x self.y))
               :update (fn [self dt])
            :handlers {}})

(local obj-mt {:__index obj})

(fn create-obj [brush]
  (local {: px : py : pw : ph : iw : ih} brush)
  (setmetatable {:encode brush
                 :name :obj
                 :quad (love.graphics.newQuad px py pw ph iw ih)
                 :x brush.x :y brush.y
                 :w brush.w :h brush.h}
                obj-mt))

(local object-brushes
       {:player {:name :player
                 :px 128
                 :py 16
                 :pw 16
                 :ph 16
                 :iw 256
                 :ih 256
                 :cx 4
                 :cy 8
                 :cw 8
                 :ch 8
                 :w 16
                 :h 16}
        :obj {:name :obj
              :px 128
              :py 32
              :pw 32
              :ph 32
              :iw 256
              :ih 256
              :cx 4
              :cy 8
              :cw 8
              :ch 8
              :w 32
              :h 32}})

(local ground-brushes
       {:ground {:name :ground
                 :bitmap :bitmap-3x3-simple-alt
                 ;; :bitmap-2x2 ;;:bitmap-3x3-simple-alt
                 :bitmap-w 4
                 :char :1
                 :ix 0
                 :iy 0}
        :chasm {:name :chasm
                :bitmap :bitmap-1
                :bitmap-w 1
                :char :0
                :ix 4
                :iy 4}})

(local l1 (level.create :test 256 256 {:x 16 :y 16} atlas {:player create-player
                                                           :obj create-obj}))

(-> l1    
    (l1.add-layer :ground :tile ground-brushes atlas)
    (l1.add-layer :objects :objects object-brushes)    
    (l1.add :objects 2 2 :player)
    (l1.add :objects 100 30 :obj)
    (l1.add :ground 4 4 :ground)
    (l1.add :ground 4 4 :chasm true)    
    (l1.add :ground 3 3 :ground)
    (l1.add :ground 3 4 :ground)
    (l1.add :ground 3 5 :ground)
    (l1.add :ground 3 6 :ground)
    (l1.add :ground 4 3 :ground)
    (l1.add :ground 4 4 :ground)
    (l1.add :ground 4 5 :ground)    
    (l1.add :ground 4 6 :ground)
    (l1.add :ground 4 7 :ground)
    (l1.add :ground 5 3 :ground)
    (l1.add :ground 5 4 :ground)
    (l1.add :ground 5 7 :ground)
    (l1.add :ground 5 6 :ground true)
    )

(l1:save :sample-level.fnl)

(local l2 (level.load-map :sample-level {:player create-player
                                         :obj create-obj}))

(local camera {:x 0 :y 0 :scale 4})

(fn update-camera []
  (tset camera :y (- camera.y
                   (if (love.keyboard.isDown :down) 1 0)
                   (if (love.keyboard.isDown :up) -1 0)))
  (tset camera :x (- camera.x (if (love.keyboard.isDown :left) -1 0)
                   (if (love.keyboard.isDown :right) 1 0)))
  )

(fn love.update [dt]
  (l2:update-layer :objects dt)
  (l2:y-sort-layer :objects)
  (update-camera)
  (editor.update dt camera)  
  )


(editor.init l2 {:x 0 :y 0 :scale 4})
(fn love.draw []
  ;;(love.graphics.draw l1.image)
  (love.graphics.push)
  (love.graphics.scale camera.scale)
  (love.graphics.translate camera.x camera.y)
  (l2:draw-layer :ground)
  (l2:draw-layer :objects)  
  (love.graphics.pop)
  (editor.draw)
  )

;; implement this in interface
(fn love.mousepressed [x y button]
  ;; (let [px (math.floor (+ 1 (/ x 16 4)))
  ;;       py (math.floor (+ 1 (/ y 16 4)))]
  ;;   (pp button)
  ;;   (if (= 1 button)
  ;;       (l2:add :ground px py :ground true)
  ;;       (l2:add :ground px py :chasm true))
  ;;   )
  ;; (pp l2.layers.ground.data)
  (local (t c) (editor.mousepressed x y button))

  )

(fn love.mousereleased [x y button]
  (local (t c) (editor.mousereleased x y button)))

(fn love.keypressed [key code]
  (match key
    :s (l2:save :sample-level-2.fnl)
    :1 (tset camera :scale 1)
    :2 (tset camera :scale 2)
    :3 (tset camera :scale 3)
    :4 (tset camera :scale 4)
    :5 (tset camera :scale 5)
    :6 (tset camera :scale 6)
    _ (editor.keypressed key code)))
