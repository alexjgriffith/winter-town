(local map (require :map))

(macro second [f]
  `(let [(_# second#) ,f] second#))

(macro db [note str]
  (when true ;; t0x02
    `(let [fennel# (require :fennel)]
       (print ,note (fennel#.view ,str)))))

(macro mock [name fun]
  (when true ;; t0x01
    `(local ,name ,fun)))

(macro test [name fun comp? result]
  (when true ;; t0x01
    `(do
       (local fennel# (require :fennel))
       (local comp#
              (or (and (~= ,comp? :=) ,comp?)
                  (fn [ga# gb#]                           
                    (= (fennel#.view ga#) ( fennel#.view gb#)))))
       (local result# ,fun)
       (print  (fennel#.view
               [,name (if (comp# result# ,result)
                          :pass
                          (values :fail result# ,result))])))))

;; (fn map.empty [name w h tilesize atlas]
;;   (local tw (math.floor (/ w tilesize)))
;;   (local th (math.floor (/ h tilesize)))
;;   {:name name
;;    :w w
;;    :h h
;;    :tw tw
;;    :th th
;;    :atlas atlas
;;    :tilesize tilesize
;;    :layers {}})

;; ;; :ground {:type :tile
;; ;;                      :image atlas
;; ;;                      :encoded-data (zeros tw th)
;; ;;                      :brushes {:ground {:name :ground
;; ;;                                         :bitmap :bitmap-1
;; ;;                                         :bitmap-w 1
;; ;;                                         :char :1
;; ;;                                         :ix 0
;; ;;                                         :iy 0}
;; ;;                                :chasm {:name :chasm
;; ;;                                        :bitmap :bitmap-1
;; ;;                                        :bitmap-w 1
;; ;;                                        :char :0
;; ;;                                        :ix 1
;; ;;                                        :iy 0}}}
;; ;;             :objects {:type :object
;; ;;                       :image atlas
;; ;;                       :encoded-data [{:name :player :x 10 :y 10 :ix 0 :iy 0}]
;; ;;                       :brushes {:player {:name :player :ix 2 :iy 0 :iw 1 :ih 1}}}


(mock check-map (map.empty :sample.fnl 256 256 16 :atlas.png))

(mock tile-brush {:name :brush
                  :bitmap :bitmap-1
                  :bitmap-w 1
                  :char :0
                  :ix 0
                  :iy 0})

(mock test-brush {:name :test-brush
                  :bitmap :bitmap-1
                  :bitmap-w 1
                  :char :1
                  :ix 0
                  :iy 0})

(mock object-brush {:name :player :ix 2 :iy 0 :iw 1 :ih 1 :w 16 :h 16})

(test :map.image (. check-map :atlas) _G.= :atlas.png)

(test :map.add-layer-tile
      (second
       (map.add-layer check-map :test :tile {tile-brush.name tile-brush} :image.png "0" ))
      := {:brushes {tile-brush.name tile-brush}
           :encoded-data "0"
           :image "image.png"
           :type "tile"})

(test :map.add-layer-ibj
      (second
       (map.add-layer check-map :objects :object {object-brush.name object-brush} :image.png [] ))
      := {:brushes {object-brush.name object-brush}
           :encoded-data []
           :image "image.png"
           :type :object})

(test :map.add-brush (. (map.add-brush check-map :test test-brush.name test-brush) :layers :test)
      :=
      {:brushes {tile-brush.name tile-brush test-brush.name test-brush}
       :encoded-data "0"
       :image "image.png"
       :type "tile"})

(test :map.set-layer-image (. (map.set-layer-image check-map :test :new-image-filename) :layers :test :image) := :new-image-filename)

(test :map.set-name (. (map.set-name check-map :test-name) :name) := :test-name)

(mock test-obj-callbacks
      {:player
       (fn [brush ...]
         (local {:x x :y y : w : h :name name} brush)
         (local player {})
         (fn player.serialize [self] self.decode)
         (local player-mt {:__index player})
         (local ret {:decode brush :x x :y y : w : h :name name})
         (setmetatable ret player-mt))})

(test :map.add-object-encoded (. (map.add-object-encoded check-map :objects 1 1 object-brush.name test-obj-callbacks) :layers :objects :encoded-data 1)
      :=
      {:h 16 :ih 1 :iw 1 :ix 2 :iy 0 :name "player" :w 16 :x 1 :y 1})

(test :map.add-object (. (map.add-object check-map :objects 1 1 object-brush.name test-obj-callbacks) :layers :objects :data 1)
      :=
      {:decode {:ih 1
                :iw 1
                :ix 2
                :iy 0
                :name "player"
                :x 1
                :y 1
                :w 16
                :h 16
                }
       :name "player"
       :x 1
       :y 1
       :w 16
       :h 16})

(test :map.remove-object
      (. (map.remove-object check-map :objects 2 2)
         :layers :objects :data)
      := [])

(test :map.deserialize
      (. (map.deserialize check-map test-obj-callbacks)
         :layers :objects :data 1)
      :=
      {:decode {:h 16
                :ih 1
                :iw 1
                :ix 2
                :iy 0
                :name "player"
                :w 16
                :x 1
                :y 1}
       :h 16
       :name "player"
       :w 16
       :x 1
       :y 1})

(test :map.clear-encoded-data (. (map.clear-encoded-data check-map) :layers :test :encoded-data)
      := nil)

(test :map.serialize (. (map.serialize check-map) :layers :test :encoded-data)
      := "0")

(test :map.replace-tile
      (-> check-map
          (map.deserialize test-obj-callbacks)
          (map.replace-tile :test 1 1 :test-brush true)
          (map.serialize)
          (. :layers :test :encoded-data)
          )
      :=
      "1")

(test :map.get-layers (map.get-layers check-map) := ["test" "objects"])

(test :map.get-brushes (map.get-brushes check-map :test) := {tile-brush.name tile-brush test-brush.name test-brush})
