local level = {}
local cwd = ((...):gsub("%.level$", "") .. ".")
local map = require((cwd .. "map"))
level["load-map"] = function(map_file, callbacks, previews)
  local map_data
  if (love.filesystem.isFused() and love.filesystem.exists((map_file .. ".fnl"))) then
    pp(("Game is Fused: Loading from " .. map_file .. ".fnl"))
    map_data = fennel.eval(love.filesystem.read((map_file .. ".fnl")), {})
  else
    map_data = require(map_file)
  end
  return level.initialize(map.deserialize(map_data, callbacks), callbacks, previews)
end
love.handlers["edit-level"] = function(key, ...)
  return pp({key, ...})
end
level.add = function(map_data, layer_name, x, y, brush_name, reautotile_3f)
  local _let_2_ = map_data
  local object_creation_callbacks = _let_2_["object-creation-callbacks"]
  local layer = map["get-layer"](map_data, layer_name)
  local brush = map["get-brush"](map_data, layer_name, brush_name)
  if ("tile" == layer.type) then
    map["replace-tile"](map_data, layer_name, x, y, brush_name, reautotile_3f)
    if reautotile_3f then
      level["update-spritebatch"](map_data, layer_name)
    else
    end
    love.event.push("edit-level", "replace-tile", layer_name, brush_name, x, y, map_data.tilesize)
    return map_data, brush
  else
    map["add-object"](map_data, layer_name, x, y, brush_name, object_creation_callbacks)
    love.event.push("edit-level", "add-object", layer_name, brush_name, x, y)
    return map_data, brush
  end
end
level.remove = function(map_data, layer_name, x, y)
  local _, brush, obj = map["remove-object"](map_data, layer_name, x, y)
  if obj then
    for key, value in pairs(obj) do
      if ("nil" ~= type(value)) then
        obj[key] = nil
      else
      end
    end
    pp(obj)
    love.event.push("edit-level", "remove-object", layer_name, brush.name, obj)
  else
  end
  return map_data, brush
end
level.save = function(map_data, file)
  do
    local f = assert(io.open(file, "wb"))
    local c = f:write(fennel.view(map.serialize(map_data)))
    f:close()
  end
  return nil
end
local slow = false
local function draw_tile_layer(image, quads, data, layer_name, tilesize, spritebatch)
  love.graphics.push()
  if slow then
    for _index, _7_ in ipairs(data) do
      local _each_8_ = _7_
      local x = _each_8_["x"]
      local y = _each_8_["y"]
      local bitmap_index = _each_8_["bitmap-index"]
      local ignore = _each_8_["ignore"]
      local char = _each_8_["char"]
      if (bitmap_index ~= ignore) then
        love.graphics.draw(image, quads[char][bitmap_index], ((x - 1) * tilesize.x), ((y - 1) * tilesize.y))
      else
      end
    end
  else
    love.graphics.draw(spritebatch)
  end
  return love.graphics.pop()
end
local function draw_object_layer(image, objects, ...)
  love.graphics.push()
  for _index, _11_ in ipairs(objects) do
    local _each_12_ = _11_
    local x = _each_12_["x"]
    local y = _each_12_["y"]
    local object = _each_12_
    if object.draw then
      object:draw(image, ...)
    else
    end
  end
  return love.graphics.pop()
end
level["draw-layer"] = function(map_data, layer_name, ...)
  local _local_14_ = map_data.layers[layer_name]
  local quads = _local_14_["quads"]
  local image = _local_14_["image"]
  local data = _local_14_["data"]
  local type = _local_14_["type"]
  local spritebatch = _local_14_["spritebatch"]
  local tilesize = map_data.tilesize
  do
    local _15_ = type
    if (_15_ == "tile") then
      draw_tile_layer(image, quads, data, layer_name, tilesize, spritebatch)
    elseif (_15_ == "objects") then
      draw_object_layer((image or map_data.image), data, ...)
    else
    end
  end
  return map_data
end
local function clone(obj)
  local ret = {}
  for key, value in pairs(obj) do
    ret[key] = value
  end
  return ret
end
level.preview = function(map_data, layer_name, x, y, brush_name, type, camera)
  local layer = map["get-layer"](map_data, layer_name)
  local brush = clone(map["get-brush"](map_data, layer_name, brush_name))
  do end (brush)["x"] = x
  brush["y"] = y
  local _17_ = type
  if (_17_ == "object") then
    local obj = (map_data["object-preview-callbacks"])[brush.name](brush)
    love.graphics.push()
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.scale(camera.scale)
    love.graphics.translate(camera.x, camera.y)
    draw_object_layer((layer.image or map_data.image), {obj})
    return love.graphics.pop()
  elseif (_17_ == "tile") then
    local _let_18_ = brush
    local char = _let_18_["char"]
    local _let_19_ = map_data
    local tilesize = _let_19_["tilesize"]
    local image = (layer.image or map_data.image)
    local quads = layer.quads
    return love.graphics.draw(image, quads[char][1], ((x - 1) * tilesize.x), ((y - 1) * tilesize.y))
  else
    return nil
  end
end
local function update_object_layer(dt, data, ...)
  for _, obj in ipairs(data) do
    if obj.update then
      obj:update(dt, ...)
    else
    end
  end
  return nil
end
local function layer_operation(operation, dt, data, ...)
  for _, obj in ipairs(data) do
    if obj[operation] then
      obj[operation](obj, dt, ...)
    else
    end
  end
  return nil
end
level["update-layer"] = function(map_data, layer_name, dt, ...)
  local _local_23_ = map_data.layers[layer_name]
  local data = _local_23_["data"]
  local type = _local_23_["type"]
  local _24_ = type
  if (_24_ == "tile") then
    return "nil"
  elseif (_24_ == "objects") then
    return update_object_layer(dt, data, ...)
  else
    return nil
  end
end
level.operation = function(map_data, operation, layer_name, dt, ...)
  local _local_26_ = map_data.layers[layer_name]
  local data = _local_26_["data"]
  local type = _local_26_["type"]
  local _27_ = type
  if (_27_ == "tile") then
    return "nil"
  elseif (_27_ == "objects") then
    return layer_operation(operation, dt, data, ...)
  else
    return nil
  end
end
level["enter-layer"] = function(map_data, layer_name, dt, ...)
  local _local_29_ = map_data.layers[layer_name]
  local data = _local_29_["data"]
  local type = _local_29_["type"]
  local _30_ = type
  if (_30_ == "tile") then
    return "nil"
  elseif (_30_ == "objects") then
    return layer_operation("enter", dt, data, ...)
  else
    return nil
  end
end
level["leave-layer"] = function(map_data, layer_name, dt, ...)
  local _local_32_ = map_data.layers[layer_name]
  local data = _local_32_["data"]
  local type = _local_32_["type"]
  local _33_ = type
  if (_33_ == "tile") then
    return "nil"
  elseif (_33_ == "objects") then
    return layer_operation("leave", dt, data, ...)
  else
    return nil
  end
end
level["mousepressed-layer"] = function(map_data, layer_name, dt, ...)
  local _local_35_ = map_data.layers[layer_name]
  local data = _local_35_["data"]
  local type = _local_35_["type"]
  local _36_ = type
  if (_36_ == "tile") then
    return "nil"
  elseif (_36_ == "objects") then
    return layer_operation("mousepressed", dt, data, ...)
  else
    return nil
  end
end
local map_mt = {__index = map}
setmetatable(level, map_mt)
local level_mt = {__index = level}
local function quad_regions_to_quads(quad_regions)
  local ret = {}
  for name, regions in pairs(quad_regions) do
    ret[name] = {}
    for key, _38_ in pairs(regions) do
      local _each_39_ = _38_
      local x = _each_39_["x"]
      local y = _each_39_["y"]
      local w = _each_39_["w"]
      local h = _each_39_["h"]
      local iw = _each_39_["iw"]
      local ih = _each_39_["ih"]
      ret[name][key] = love.graphics.newQuad(x, y, w, h, iw, ih)
    end
  end
  return ret
end
level["set-quads"] = function(map_data, layer_name, ...)
  local layer = map_data.layers[layer_name]
  local _let_40_ = layer
  local type = _let_40_["type"]
  local data = _let_40_["data"]
  layer["quads"] = {}
  do
    local _41_ = type
    if (_41_ == "tile") then
      local image = (layer.image or map_data.image)
      local iw, ih = image:getDimensions()
      do end (layer)["quads"] = quad_regions_to_quads(map["quad-regions"](map_data, layer_name, iw, ih, ...))
    elseif (_41_ == "objects") then
    else
    end
  end
  return map_data
end
level["update-spritebatch"] = function(map_data, layer_name)
  local _let_43_ = map["get-layer"](map_data, layer_name)
  local spritebatch = _let_43_["spritebatch"]
  local quads = _let_43_["quads"]
  local data = _let_43_["data"]
  local layer = _let_43_
  spritebatch:clear()
  local tilesize = map_data.tilesize
  for _, _44_ in ipairs(data) do
    local _each_45_ = _44_
    local char = _each_45_["char"]
    local x = _each_45_["x"]
    local y = _each_45_["y"]
    local bitmap_index = _each_45_["bitmap-index"]
    local d = _each_45_
    spritebatch:add(quads[char][bitmap_index], ((x - 1) * tilesize.x), ((y - 1) * tilesize.y))
  end
  return nil
end
level["add-layer"] = function(map_data, layer_name, ...)
  map.deserialize(map["add-layer"](map_data, layer_name, ...), map_data["object-creation-callbacks"])
  local layer = map_data.layers[layer_name]
  if layer.atlas then
    layer["image"] = love.graphics.newImage(layer.atlas)
  else
  end
  if layer.data then
    level["set-quads"](map_data, layer_name)
  else
  end
  if ("tile" == layer.type) then
    level["add-spritebatch"](map_data, layer_name)
  else
  end
  return map_data, layer
end
level["add-spritebatch"] = function(map_data, layer_name)
  local layer = map["get-layer"](map_data, layer_name)
  local _local_49_ = map_data
  local tw = _local_49_["tw"]
  local th = _local_49_["th"]
  local image = _local_49_["image"]
  local sb = love.graphics.newSpriteBatch((layer.image or image), (tw * th))
  do end (layer)["spritebatch"] = sb
  level["update-spritebatch"](map_data, layer_name)
  return map_data
end
level.initialize = function(map_data, callbacks, previews)
  local _local_50_ = map_data
  local name = _local_50_["name"]
  local w = _local_50_["w"]
  local h = _local_50_["h"]
  local tilesize = _local_50_["tilesize"]
  local atlas = _local_50_["atlas"]
  map_data["object-creation-callbacks"] = callbacks
  map_data["object-preview-callbacks"] = previews
  local tw, th = (w / tilesize.x), (h / tilesize.y)
  do end (map_data)["image"] = love.graphics.newImage(atlas)
  for layer_name, layer in pairs(map_data.layers) do
    if layer.atlas then
      layer["image"] = love.graphics.newImage(layer.atlas)
    else
    end
    if layer.data then
      level["set-quads"](map_data, layer_name)
    else
    end
    local _53_ = layer.type
    if (_53_ == "tile") then
      level["add-spritebatch"](map_data, layer_name)
      for _, d in ipairs(layer.data) do
        love.event.push("edit-level", "replace-tile", layer_name, d.brush, d.x, d.y, map_data.tilesize)
      end
    elseif (_53_ == "objects") then
    else
    end
  end
  return setmetatable(map_data, level_mt)
end
level.create = function(name, w, h, tilesize, atlas, callbacks, previews)
  local tilesize_table
  if ("table" == type(tilesize)) then
    tilesize_table = tilesize
  else
    tilesize_table = {x = tilesize, y = tilesize}
  end
  local map_data = map.deserialize(map.empty(name, w, h, tilesize_table, atlas), callbacks)
  return level.initialize(map_data, callbacks, previews)
end
return level
