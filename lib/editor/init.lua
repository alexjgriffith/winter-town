local editor = {__TITLE = "editor.fnl", __DESCRIPTION = "A level editor for love", __AUTHOR = "AlexJGriffith", __VERSION = "0.1.0", __LICENCE = "GPL3+"}
local cwd = ((...):gsub("%.init$", "") .. ".")
local level = require((cwd .. "level"))
local _local_1_ = require((cwd .. "interface"))
local brush = _local_1_["brush"]
local editor0 = _local_1_["editor"]
print("loading editor - main.fnl")
do end (editor0)["brush"] = brush
level["editor"] = editor0
return level
