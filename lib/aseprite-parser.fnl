(local json (require :lib.json))
(local anim8 (require :lib.anim8))

;; save the aseprite export data as json with the
;; layout "T:{title} L:{layer} F:{Frame}"
(macro db [str]
  (when false
    `(pp ,str)))

(local ap {})

(fn ap.parse [file]
  (local filecontents  (love.filesystem.read file))
  (local data (json.decode filecontents))
  (local image (love.graphics.newImage 
                (.. "assets/sprites/" data.meta.image)))
  (local iw data.meta.size.w)
  (local ih data.meta.size.h)
  (local ret {:image "" :frames {} :slices {} :tags {}
              :layers {} : iw : ih :oneframe false})

  (local frames data.frames)
  (var w 0)
  (var h 0)
  (db file)
  (each [title {: frame : sourceSize : duration} (pairs frames)]
    (local name
           (string.sub (string.match title "T:.*%sL:") 3 -4))
    (local layerName
           (string.sub (string.match title "L:.*%sF:") 3 -4))
    (local frameName
           (string.sub (string.match title "F:.*") 3))
    (local f {:x frame.x
           :y frame.y
           :w frame.w
           :h frame.h
           : iw 
           : ih
           :duration duration
           :layer layerName
           :frame frameName
           :name name
           :title title
           })
    (when (not ret.oneframe)
      (tset ret :oneframe f))
    (tset ret.frames title f)
    )
  (tset ret :image image)
  (each [_ {: name : from : to} (ipairs data.meta.frameTags)]
    (tset ret.tags name {: name : from : to}))
  (each [_ {: name } (ipairs data.meta.layers)]
    (table.insert ret.layers {: name}))
  (each [_ {: name : colour : keys} (ipairs data.meta.slices)]
    (tset ret.slices name {: name : colour
                         :x (. keys 1 :bounds :x)
                         :y (. keys 1 :bounds :y)
                         :w (. keys 1 :bounds :w)
                         :h  (. keys 1 :bounds :h)
                         : iw
                         : ih}))
  ret)

(local quads {})
(fn quads.draw [self name ...]
  (love.graphics.draw (. self name :image) (. self name :quad) ...))

(local quads-mt {:__index quads})

(fn ap.slices-as-quads [file pre? ox? oy?]
  (local ox (or ox? 0))
  (local oy (or oy? 0))
  (local pre (or pre? ""))
  (local data (ap.parse file))
  (db :slices-as-quads)  
  (local image data.image)
  (local slices data.slices)
  (db slices)
  (local ret {})  
  (each [name { : x : y : w : h : iw : ih} (pairs slices)]
    (local quad (love.graphics.newQuad (+ x ox) (+ y oy) w h iw ih))
    (db (.. "QUAD NAME:" name))
    (tset ret (.. pre name) {: image : quad}))
  (db ret)
  (setmetatable ret quads-mt))

(fn ap.layers-as-quads [file]
  (local data (ap.parse file))
  (local image data.image)
  (local slices data.slices)
  (local layers data.layers)
  (db :layers-as-quads)
  (db layers)
  (local {: w : h : iw : ih : layerName : name} (. data :oneframe))
  (local ret {})
  (each [_ {:name layerName} (ipairs layers)]
    (local frameName (.. "T:" name " L:" layerName " F:0"))
    (db frameName)
    (local {: x : y : w : h} (. data.frames frameName))
    (local quad (love.graphics.newQuad x y w h iw ih))
    (tset ret layerName {: image : quad}))
  (setmetatable ret quads-mt))

(fn ap.layers-slices-as-quads [file]
  (local data (ap.parse file))
  (local image data.image)
  (local slices data.slices)
  (local layers data.layers)
  (db :slices-as-quads)
  (db layers)
  (local {: w : h : iw : ih : layerName : name} (. data :oneframe))
  (local ret {})
  (each [_ {:name layerName} (ipairs layers)]
    (local frameName (.. "T:" name " L:" layerName " F:0"))
    (local {: x : y} (. data.frames frameName))
    (local slices (ap.slices-as-quads file layerName x y))
    (each [name value (pairs slices)] (tset ret name value)))
  (setmetatable ret quads-mt))

;; example usage
;; (local sprites :some-file.json)
;; (sprites:draw :some-sprite x y)

(fn ap.tags-as-animations [file layerName?]
  ;; (local layerName (or layerName? "Layer 1"))
  (local data (ap.parse file))
  (db :tags-as-animations)
  (db file)
  (db data)
  (local image data.image)
  (local tags data.tags)
  (local {: w : h : iw : ih :layer  layerName : name} (. data :oneframe))
  (local g (anim8.newGrid w h iw ih))
  (local ret {})
  (each [tagName {: from : to} (pairs tags)]
    (local durations [])
    (for [i from to]
      (local frameName (.. "T:" name " L:" layerName " F:" i))
      (table.insert durations (/ (. data.frames frameName :duration) 1000)))
    (db [durations (.. (+ from 1) "-" (+ to 1))])
    (local animation (anim8.newAnimation
                      (g (.. (+ from 1) "-" (+ to 1)) 1)
                      durations))
    (tset ret tagName {: image : animation}))
  (db ret)
  ret)

ap
