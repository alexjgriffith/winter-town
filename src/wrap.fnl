(local gamestate (require :lib.gamestate))
(local stdio (require :lib.stdio))
(local state (require :state))
(local fade (require :lib.fade))

(global preview false)

(fn love.load [args]
  (love.graphics.setDefaultFilter "nearest" "nearest" 0)
  (if (= :web (. args 1)) (global web true) (global web false))
  (global resources (require :resources))  
  (resources.initialize)
  (love.mouse.setCursor resources.cursor)
  (resources.footsteps:setLooping true)
  (resources.footsteps:setVolume 1)
  (resources.bgm:play)
  (local interactions (require :interactions))
  (fade.set-colour resources.black)
  (gamestate.registerEvents)
  (local levels (require :load-levels))
  (set state.levels levels)
  (set state.level levels.map)
  (gamestate.switch (require :mode-menu))
  (when (not web) (stdio.start)))

(fn love.update [dt]
  (fade.update (math.min dt 0.032)) ;; don't tie to frame rate
  )
