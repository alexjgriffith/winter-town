(local maplayer {})
(local object (require :object))
(setmetatable maplayer {:__index object})
(local lg love.graphics)

(fn maplayer.draw [self image]
  (local {: image : quad} (. _G.resources :atlas self.sprite))
  (lg.setColor 1 1 1 1)
  (love.graphics.draw image quad self.x self.y))

(fn maplayer.interact [self player])

(fn maplayer.update [self dt])

(local maplayer-mt {:__index maplayer})

(fn maplayer.preview [brush]
  (local {: x : y  : name : sprite} brush)
  (setmetatable
          {: sprite
           :x x           
           :y y} maplayer-mt))

(fn maplayer.create [brush]
  (local {: x : y :  name : sprite} brush)
  (local obj
         (setmetatable
          {:brush brush
           : name
           : sprite
           :w 448
           :h 464
           :x x
           :y y
           :type :maplayer
           } maplayer-mt))
  obj)

maplayer
