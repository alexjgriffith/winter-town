(local darkness {})
(local object-base (require :object))
(setmetatable darkness {:__index object-base})
(local lg love.graphics)

(fn darkness.draw [self]
  (when self.canvas
    (lg.setColor resources.black)
    (lg.draw self.canvas)
    (lg.setColor 1 1 1 1)))

(fn darkness.interact [self player])


(fn y-offset [y who]
  (+ y (match who
         :light 4
         :smalllight 4
         :fire 8
         :player 8
         :firePlace 16
         _ 0)))

(fn x-offset [x who]
  (+ x (match who
         :light 4
         :smalllight 4
         :fire 8
         :player 8
         :firePlace 16
         _ 0)))
(fn darkness.update [self dt]
  (local data (. (require :state) :level :layers :objects :data))
  (local lights (lume.filter data (fn [d] (or (= :light d.type)
                                              d.fire
                                              (= :player d.name)))))
  (lg.push)
  (lg.setCanvas self.canvas)
  (lg.clear 1 1 1 self.alpha)
  (love.graphics.setBlendMode :replace)
  (lg.setColor 1 1 1 0.9)
  (each [_ l (ipairs lights)]
    (local r l.fire.radius)
    (local n l.name)
    (lg.circle :fill (x-offset l.x n) (y-offset l.y n) (* 40 r) 8))
  (lg.setColor 1 1 1 0.7)
  (each [_ l (ipairs lights)]
    (local r l.fire.radius)
    (local n l.name)
    (lg.circle :fill (x-offset l.x n) (y-offset l.y n) (* 28 r) 8))
    (lg.setColor 1 1 1 0.5)
  (each [_ l (ipairs lights)]
        (local r l.fire.radius)
    (local n l.name)
    (lg.circle :fill (x-offset l.x n) (y-offset l.y n) (* 35 r) 8))
  (lg.setColor 1 1 1 0)
  (each [_ l (ipairs lights)]
        (local r l.fire.radius)
    (local n l.name)
    (lg.circle :fill (x-offset l.x n) (y-offset l.y n) (* 20 r) 8))
  (lg.setBlendMode :alpha)
  (lg.setCanvas)
  (lg.pop)
  )

(local darkness-mt {:__index darkness})

(fn darkness.preview [brush])

(fn darkness.create [brush]
  (local {: x : y : w : h  : alpha : name }
         brush)
  (local canvas (lg.newCanvas w h))
  (local obj
         (setmetatable
          {:brush brush
           : name
           : canvas
           : alpha
           :w w
           :h h
           :x x
           :y y
           :type :darkness
           } darkness-mt))
  obj)

darkness
