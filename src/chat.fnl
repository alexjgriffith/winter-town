(local chat {})

(local gamestate (require :lib.gamestate))

(local fade (require :lib.fade))
(var click-down false)
(var hovered false)
(var mute false)
(local lg love.graphics)

(local char-space 10)
(local line-space 5)

(var last-text "")

(var char-point 0)
(var char-len 0)
(var global-dt 0.016)
(var hover-reset true)

(var who false)

(local interactions (require :interactions))

(fn chat.close []
  (let [state (require :state)]
    (set state.ui false)
    (when (and state.chat state.chat.callback) (state.chat.callback))))

(fn click [text question dx]
  (local down (and (= 0 dx) (love.mouse.isDown 1)))
  (if down
      (when (not click-down)
        (resources.click:stop)
        (resources.click:play)
        (match text
          :Back (chat.close)
          )
        (when (and who question)
          (local state (require :state))
          (when (and state.chat state.chat.callback) (state.chat.callback))
          (interactions.question who question))
        (set click-down true)
        )
      (do
        (when (~= text hovered)
          (do (set hovered text)
              (resources.hover:stop)
              (resources.hover:play)))
        (set click-down false))))

(fn draw-small-button [text mx my x y image upquad downquad iconquad dx]
  (local (bw bh) (values 10 10))
  (lg.setColor 1 0 0 1)
  (lg.setColor 1 1 1 1)
  (if (pointWithin mx my (+ x 2) (+ y 2) bw bh)
      (do (lg.draw image upquad x y)
          (click text nil dx)
          (set hover-reset false)
          (lg.draw image iconquad (+ x 4) (+ y 4)))
      (do (lg.draw image downquad x y)          
          (lg.draw image iconquad (+ x 4) (+ y 3)))))

(fn draw-half-button [text question  mx my x y dx]
  (local (bw bh) (values 44 10))
  (local {: image :quad downquad} (. resources.atlas :UITextButton3Down))
  (local {:quad upquad} (. resources.atlas :UITextButton3Up))
  (lg.setFont resources.text-font)
  (fn draw-text [text x y]
    (lg.setColor resources.black)
    (lg.print text (+ 1 x) (+ y 1))
    (lg.setColor 1 1 1 1))
  (if (pointWithin mx my (+ x 2) (+ y 1) bw bh)
        (do (lg.draw image downquad x y)
            (click text question dx)
            (set hover-reset false)
            (draw-text text x y))
        (do (lg.draw image upquad x y)
            (draw-text text x (- y 1)))))


(fn draw-button [text question mx my x y  dx]
  (local (bw bh) (values 64 10))
  (local {: image :quad downquad} (. resources.atlas :UITextButtonDown))
  (local {:quad upquad} (. resources.atlas :UITextButtonUp))
  (lg.setFont resources.text-font)
  (fn draw-text [text x y]
    (lg.setColor resources.black)
    (lg.printf text (+ 1 x) (+ y 1) (- bw 4) :left)
    (lg.setColor 1 1 1 1))
  (if (pointWithin mx my (+ x 2) (+ y 1) bw bh)
        (do (lg.draw image downquad x y)
            (click text question dx)
            (set hover-reset false)
            (draw-text text x y))
        (do (lg.draw image upquad x y)
            (draw-text text x (- y 1)))))

(fn three-questions [questions mx my dx]
  (draw-button (. questions 1 :text) (. questions 1) mx my 54 90 dx)
  (when (. questions 2)
    (draw-button (. questions 2 :text) (. questions 2) mx my 54 101 dx))
  (when (. questions 3)
    (draw-button (. questions 3 :text) (. questions 3) mx my 54 112 dx)))

(fn six-questions [questions mx my dx]
  (draw-button (. questions 1 :text) (. questions 1) mx my 24 90 dx)
  (draw-button (. questions 2 :text) (. questions 2) mx my 24 101 dx)
  (draw-button (. questions 3 :text) (. questions 3) mx my 24 112 dx)
  (when (. questions 4)
    (draw-button (. questions 4 :text) (. questions 4) mx my 92 90 dx))
  (when (. questions 5)
    (draw-button (. questions 5 :text) (. questions 5) mx my 92 101 dx))
  (when (. questions 6)
    (draw-button (. questions 6 :text) (. questions 6) mx my 92 112 dx)))

(fn nine-questions [questions mx my dx]
  (local x 22)
    (draw-half-button (. questions 1 :text) (. questions 1) mx my x 90 dx)
  (draw-half-button (. questions 2 :text) (. questions 2) mx my x 101 dx)
  (draw-half-button (. questions 3 :text) (. questions 3) mx my x 112 dx)
  (when (. questions 4)
    (draw-half-button (. questions 4 :text) (. questions 4) mx my (+ x 44 2) 90 dx))
  (when (. questions 5)
    (draw-half-button (. questions 5 :text) (. questions 5) mx my (+ x 44 2) 101 dx))
  (when (. questions 6)
    (draw-half-button (. questions 6 :text) (. questions 6) mx my (+ x 44 2) 112 dx))
  (when (. questions 7)
    (draw-half-button (. questions 7 :text) (. questions 7) mx my (+ x 88 4) 90 dx))
  (when (. questions 8)
    (draw-half-button (. questions 8 :text) (. questions 8) mx my (+ x 88 4) 101 dx))
  (when (. questions 9)
    (draw-half-button  (. questions 9 :text) (. questions 9) mx my (+ x 88 4) 112 dx)))

(fn render-questions [questions mx my dx]
  ;;(pp questions)
  (local q questions)
  (if (= (# q) 0) :nil
      (< (# q) 4) (three-questions q mx my dx)
      (< (# q) 7) (six-questions q mx my dx)
      (nine-questions q mx my dx)
    )
  )

(fn chat.draw [mx my dx]
  (local state (require :state))
  (when state.chat
    (set who state.chat.name)
    (when (~= last-text state.chat.text)
      (set last-text state.chat.text)
      (set char-point 0)
      (set char-len (# state.chat.text)))
    (if (< char-point char-len)
        (do
          (resources.talk:setVolume 0.1)
          (set char-point (+ char-point (* 30 global-dt))))
        (resources.talk:setVolume 0)))
  (local str (string.sub last-text 0 (math.floor char-point)))
  (set hover-reset true)
  (local {: image :quad downSmallQuad} (. resources.atlas :UIButtonSmallUp))
  (local {:quad upSmallQuad} (. resources.atlas :UIButtonSmallDown))
  (local {:quad xQuad} (. resources.atlas :UIX))
  (draw-small-button :Back mx my (+ 48 dx 110) 110 image upSmallQuad downSmallQuad xQuad dx)
  (when (<= char-len char-point)
    (render-questions state.chat.questions mx my dx))
  (lg.setColor resources.black)
  (lg.setFont resources.text-font)
  (lg.printf str (+ 34 dx) 3 110)
  (when hover-reset
    (set hovered false))
  )

(fn chat.next []
  (local state (require :state))
  (if (< char-point char-len)
      (set char-point (math.min char-len (+ 20 char-point)))
      (and who state.chat state.chat.questions (> (# state.chat.questions) 0))
      (do
          (resources.click:stop)
          (resources.click:play)  
          (interactions.question who (. state.chat.questions 1)))
      (do
          (resources.click:stop)
          (resources.click:play)  
          (chat.close))
      )
  )

(fn chat.update [dt]
  (local state (require :state))
  (set global-dt dt)
    (when (~= state.ui :chatbox)
    (set last-text "")
    (set char-point 0)
    (resources.talk:setVolume 0)
    (set state.chat nil)              
    )
  )

chat
