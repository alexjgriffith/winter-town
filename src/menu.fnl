(local menu {})

(local gamestate (require :lib.gamestate))

(local fade (require :lib.fade))
(var click-down false)
(var hovered false)
(var mute false)
(local lg love.graphics)
(var hover-reset true)

(fn click [text dx]
  (local down (and (= 0 dx) (love.mouse.isDown 1)))
  (if down
      (when (not click-down)
        (resources.click:stop)
        (resources.click:play)
        (match text
          :Menu (fade.out
                 (fn []
                   (local state (require :state))
                 (set state.ui nil)
                   (gamestate.switch (require :mode-menu))))
          :Reset (fade.out (fn []
                             (local state (require :state))
                             (local player (. state :player))
                             (state.levels:set-level :map 167 164)
                             (set state.fresh true)
                             (set state.target.snap true)
                             (tset state :ui nil)
                             (tset state :message nil)
                             (tset state :clues [])
                             (lume.hotswap  :interactions)
                             (gamestate.switch (require :mode-menu))))
          :Credits (fade.out (fn []                             
                             (gamestate.switch (require :mode-credits))))
          :Mute (do (love.audio.setVolume (if mute 1 0)) (set mute (not mute)))
          :Back (let [state (require :state)] (set state.ui false))
          )
        (set click-down true)
        )
      (do
        (when (and (~= text hovered) (= 0 dx))
          (do (set hovered text)
              (resources.hover:stop)
              (resources.hover:play)))
        (set click-down false))))

(fn draw-button [text mx my x y image upquad downquad dx]
  (local (bw bh) (values 78 15))
  (fn draw-text [text x y]
    (lg.setColor resources.black)
    (lg.printf text (+ x 2) (+ y 1) (- bw 4) :center)
    (lg.setColor 1 1 1 1))
  (if (pointWithin mx my (+ x 2) (+ y 1) 78 15)
        (do (lg.draw image downquad x y)
            (click text dx)
            (set hover-reset false)
            (draw-text text x y))
        (do (lg.draw image upquad x y)
            (draw-text text x (- y 1)))))

(fn draw-small-button [text mx my x y image upquad downquad iconquad dx]
  (local (bw bh) (values 10 10))
  (lg.setColor 1 0 0 1)
  (lg.setColor 1 1 1 1)
  (if (pointWithin mx my (+ x 2) (+ y 2) bw bh)
      (do (lg.draw image upquad x y)
          (click text dx)
          (set hover-reset false)
          (lg.draw image iconquad (+ x 4) (+ y 4)))
      (do (lg.draw image downquad x y)
          (lg.draw image iconquad (+ x 4) (+ y 3)))))

(fn menu.draw [mx my dx]
  (local {: image :quad downQuad} (. resources.atlas :UIButton9Up))
  (local {:quad upQuad} (. resources.atlas :UIButton9Down))
  (local {:quad downSmallQuad} (. resources.atlas :UIButtonSmallUp))
  (local {:quad upSmallQuad} (. resources.atlas :UIButtonSmallDown))
  (local {:quad muteQuad} (. resources.atlas :UIMute))
  (local {:quad unmuteQuad} (. resources.atlas :UIUnmute))
  (local {:quad xQuad} (. resources.atlas :UIX))
  (set hover-reset true)
  (draw-button :Menu mx my (+ 48 dx) 30 image upQuad downQuad dx)
  (draw-button :Reset mx my (+ 48 dx) 50 image upQuad downQuad dx)
  (draw-button :Credits mx my (+ 48 dx) 70 image upQuad downQuad dx)
  (draw-small-button :Mute mx my (+ 48 dx) 90 image upSmallQuad downSmallQuad
                     (if mute muteQuad unmuteQuad) dx)
  (draw-small-button :Back mx my (+ 48 dx 64) 90 image upSmallQuad downSmallQuad xQuad dx)
  (when hover-reset
    (set hovered false))
  )

menu
