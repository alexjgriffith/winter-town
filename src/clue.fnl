(local clue {})

(local gamestate (require :lib.gamestate))

(local fade (require :lib.fade))
(var click-down false)
(var hovered false)
(var mute false)
(local lg love.graphics)

(fn click [text dx]
  (local down (and (= 0 dx) (love.mouse.isDown 1)))
  (if down
      (when (not click-down)
        (resources.click:stop)
        (resources.click:play)
        (match text
          :Back (let [state (require :state)] (set state.ui false))
          )
        (set click-down true)
        )
      (do
        (when (~= text hovered)
          (do (set hovered text)
              (resources.hover:stop)
              (resources.hover:play)))
        (set click-down false))))

(fn draw-button [text mx my x y image upquad downquad dx]
  (local (bw bh) (values 78 15))
  (fn draw-text [text x y]
    (lg.setColor resources.black)
    (lg.printf text (+ x 2) (+ y 1) (- bw 4) :center)
    (lg.setColor 1 1 1 1))
    (if (pointWithin mx my (+ x 2) (+ y 1) 78 15)
        (do (lg.draw image downquad x y)
            (click text dx)
            (draw-text text x y))
        (do (lg.draw image upquad x y)
            (draw-text text x (- y 1)))))

(fn draw-small-button [text mx my x y image upquad downquad iconquad dx]
  (local (bw bh) (values 10 10))
  (lg.setColor 1 0 0 1)
  (lg.setColor 1 1 1 1)
  (if (pointWithin mx my (+ x 2) (+ y 2) bw bh)
      (do (lg.draw image upquad x y)
          (click text dx)
          (lg.draw image iconquad (+ x 4) (+ y 4)))
      (do (lg.draw image downquad x y)
          (lg.draw image iconquad (+ x 4) (+ y 3)))))

(local cluecanvas (lg.newCanvas 100 100)) ;; 59 52
(var x 36)
(var y 5)


(var scroll 0.1)
(local char-space 10)
(local line-space 5)
(var bar-height 50)
(local bar-max 100)

(var latched false)

(fn clue.draw [mx my dx]
  (local down (and (= 0 dx) (love.mouse.isDown 1)))
  (when (not down) (set latched false))
  (local {: image :quad downSmallQuad} (. resources.atlas :UIButtonSmallUp))
  (local {:quad upSmallQuad} (. resources.atlas :UIButtonSmallDown))
  (local {:quad xQuad} (. resources.atlas :UIX))
  (draw-small-button :Back mx my (+ 48 dx 72) 110 image upSmallQuad downSmallQuad xQuad dx)
  (local (sx sy sw sh) (values (+ dx 138) (+ 10 (* (- bar-max bar-height) scroll))
                               3 bar-height))
  (if (pointWithin mx my sx sy sw sh)
      (do (lg.setColor resources.grey)
          (when (and down (not latched)) (set latched my)))
      (do (lg.setColor resources.black)))
  (when (and down latched)
    (set scroll (lume.clamp (+ scroll (/ (- my latched) (- bar-max bar-height))) 0 1))
    (set latched my)
    )
  (lg.rectangle :fill sx sy sw sh)
  (lg.setColor 1 1 1 1)
  (lg.draw cluecanvas (+ x dx) y)
  )

(fn get-height []
  (var j 0)
  (var height 0)
  (local h (resources.text-font:getHeight))
  (local state (require :state))
  (each [i clue (ipairs state.clues)]
    (local (_ texts) (resources.text-font:getWrap clue 100))
    (each [_ t (ipairs texts)]
      (set height (+ (* h (# texts)) (* j char-space) (* i line-space)))
      (set j (+ j 1))))
  height)

(fn clue.update []
  (lg.setCanvas cluecanvas)
  (lg.clear 0 0 0 0)
  (lg.setColor resources.black)
  (local state (require :state))
  (lg.setFont resources.text-font)
  (local full-height (math.max 100 (get-height)))
  (local oy (- (* scroll (- full-height 100))))
  (var j 0)
  (each [i clue (ipairs state.clues)]
    (local (_ texts) (resources.text-font:getWrap clue 100))
    (each [_ t (ipairs texts)]
      (lg.print t 0 (+ oy (* j char-space) (* i line-space)))
      (set j (+ j 1))))
  (lg.setCanvas)
  )

clue
