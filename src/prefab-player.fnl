(local player {})
(local object-base (require :object))
(setmetatable player {:__index object-base})
(local lg love.graphics)
(local fade (require :lib.fade))
(local max-velocity 60)
(local rate 600)

(local is-down love.keyboard.isDown)

(var fading false)
(fn love.handlers.move-to [entrance]
  (local player (. (require :state) :player))
  (local levels (. (require :state) :levels))
  (when (and (not fading) entrance.warp (~= entrance.warp :nil))
    (set fading true)
    (fade.out
     (fn []
       (set fading false)
       (local type entrance.parent)
       (match type
         :house (set player.last-xy {:x player.col.x :y (+ player.col.y 5)})
         :highlayer :nil)
       (levels:set-level entrance.warp entrance.px entrance.py)
       (local player (. (require :state) :player))
       (fade.in))))
  )

(fn movement [self dt]
  (local world (. (require :state) :col))
  (let [l (if (or (is-down :left) (is-down :a)) 1 0)
        r (if (or (is-down :right) (is-down :d)) 1 0)
        u (if (or (is-down :up) (is-down :w)) 1 0)
        d (if (or (is-down :down) (is-down :s)) 1 0)
        x (- r l)
        y (- d u)
        down (> (+ l r u d) 0)
        m (math.sqrt (+ (* x x) (* y y)))
        (ux uy) (if (> m 0) (values (/ x m) (/ y m)) (values 0 0))
        velocity
        (lume.clamp (if down
                        (+ self.velocity (* dt rate))
                        (- self.velocity (* dt rate)))
                    0 max-velocity)]
    (when  (> r 0)  (set self.left false))
    (when ( > l 0)  (set self.left true))
    (set self.angle (math.atan2 uy ux))
    ;; (when (or (> l 0) (> r 0)) (set self.left (> l r)))
    ;; (when (or (> u 0) (> d 0)) (set self.up (> u d)))
    (local (px py) (values
                    (+ self.col.x (* ux velocity dt))
                    (+ self.col.y (* uy velocity dt))))
    (set self.velocity velocity)
    (fn filter [item other]
      (when (= other.type :col) :slide))
    (local (ax ay) (world:move self.col px py filter))
    (set self.x (- ax 6))
    (set self.y (- ay 12))
    (set self.col.x ax)
    (set self.col.y ay)
    self
    ))

(fn player.draw [self image]  
  (lg.setColor 1 1 1 1)
  (when self.sprite
    (local {: image : quad} (. _G.resources :atlas self.sprite))
      (love.graphics.draw image quad self.x self.y))  
  (when (not self.sprite)
    ;; (pp [:animated self.velocity])
    (if (> self.velocity 0)
        (self.run:draw self.image self.x self.y)
        (self.idle:draw self.image self.x self.y))
    ))

(fn player.draw-shadow [self]
  (local {: image : quad} (. _G.resources :atlas :AtlasShadow))
      (love.graphics.draw image quad self.x (+ 2 self.y)))

(fn player.interact [self]
  (local state (require :state))
  (local world (. (require :state) :col))
  (local (c l) (world:queryRect self.col.x self.col.y self.col.w self.col.h
                                (fn [item] (= item.type :interaction))))
  (when (> l 0)
    (when (not state.ui)
      (: (. state.mapping.interaction (. c 1)) :interact self)
      (love.event.push :interaction (. c 1)))
    ;; (pp c)
    )
  )

(var running false)
(fn player.update [self dt mode]
  (local {: update-fire} (require :util))
  (local ui (. (require :state) :ui))
  (update-fire self dt)
  (when (and (not ui) (not fading) (not (= :menu mode)))
    (movement self dt))
  (if (or ui (= mode :menu)) (set self.velocity 0))
  (self.run:update dt)
  (self.idle:update dt)
  (if self.left
      (do (set self.run.flippedH true)
          (set self.idle.flippedH true))
      (do (set self.run.flippedH false)
          (set self.idle.flippedH false)))
  (if (and (not running) (> self.velocity 0))
      (do (set running true)
          (resources.footsteps:setLooping true)
          (resources.footsteps:setVolume (or self.footstep-volume 0))
          (resources.footsteps:stop)
          (resources.footsteps:play)
          )
      (and running (= self.velocity 0))
      (do (set running false)
          (resources.footsteps:setVolume 0.1)
          (resources.footsteps:setLooping false))))

(fn player.enter [self]
  )

(fn player.leave [self]
  )

(local player-mt {:__index player})

(fn player.translate [self x y]
  (local world (. (require :state) :col))
  (world:update self.col x y)
  (set self.col.x x)
  (set self.col.y y))

(fn player.preview [brush]
  (local {: x : y  : name} brush)
  (setmetatable
          {:sprite :AtlasPlayer
           :x x           
           :y y} player-mt))



(fn player.create [brush]
  (local {: x : y :  name
          : cx : cy : cw : ch
          : w : h}
         brush)
  (local anim8 (require :lib.anim8))
  (local {:quad quadIdle : image } (. resources :atlas :AtlasPlayerIdle))
  (local {:quad quadRun } (. resources :atlas :AtlasPlayerRun))
  (local animation-canvas (lg.newCanvas (* 2 w) (* 2 h)))
  (lg.setCanvas animation-canvas)
  (lg.setColor 1 1 1 1)
  (lg.draw image quadIdle 0 0)
  (lg.draw image quadRun 0 16)
  (lg.setCanvas)
  (local grid (anim8.newGrid w h (* w 2) (* h 2)))
  (local idle (anim8.newAnimation (grid (.. 1 "-" 2) 1) 1))
  (local run (anim8.newAnimation (grid (.. 1 "-" 2) 2) 0.2))

  (local obj
         (setmetatable
          {:brush brush
           : name
           :image animation-canvas
           :run run
           :idle idle
           :x x
           :y y
           : w
           : h
           :fire {:radius 0.5 :expanding false :max-radius 0.5
                  :min-radius 0.3}
           :left false
           :up false
           :angle 0
           :velocity 0
           :has-entered false
           :col {:type :col :name name :x (+ x cx)
                  :y (+ y cy) :w cw :h ch}
           :type :player
           } player-mt))
  (local world (. (require :state) :col))
  (world:add obj.col obj.col.x obj.col.y obj.col.w obj.col.h)
  (local state (require :state))
  ;; (set state.player obj)

  obj)

player
