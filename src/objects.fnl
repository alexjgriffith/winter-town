(local player (require :prefab-player))
(local house (require :prefab-house))
(local maplayer (require :prefab-maplayer))
(local highlayer (require :prefab-highlayer))
(local object (require :prefab-object))
(local snow (require :prefab-snow))
(local footsteps (require :prefab-footsteps))
(local darkness (require :prefab-darkness))
(local light (require :prefab-light))
(local sounds (require :prefab-sounds))
(local slider (require :prefab-slider))
(local npc (require :prefab-npc))

(local t {})
(fn t.get-create [self]
  (local t [])
  (each [key value (pairs self)]
    (when (= :table (type self))
      (tset t key value.create)))
  t)

(fn t.get-preview [self]
  (local t [])
  (each [key value (pairs self)]
    (when (= :table (type self))
      (tset t key value.preview)))
  t)

(local mt {:__index t})

(setmetatable {:house1 house
               :house2 house
               :house3 house
               :house4 house
               :house5 house
               :tree object
               :player player
               :light light
               :fire light
               :smalllight light
               :room1 maplayer
               :room2 maplayer
               :room3 maplayer
               :room4 maplayer
               :room5 maplayer
               :highroom1 highlayer
               :highroom2 highlayer
               :highroom3 highlayer
               :highroom4 highlayer
               :highroom5 highlayer               
               :ground maplayer
               :road maplayer
               :lowcliff maplayer
               :highcliff highlayer
               :snow snow
               :footsteps footsteps
               :darkness darkness
               :firesound sounds
               :menu slider
               :chatbox slider
               :clue slider
               :carlington npc
               :victoria npc
               :william npc
               :alice npc
               :martin npc
               :stanley npc
               :jane npc
               :bed object
               :doublebed object
               :table object
               :counter object
               :knifeblock object
               :curtain1 object
               :curtain2 object
               :curtain3 object
               :stanleyTable object
               :samuelTable object
               :chest object
               :chairfront object
               :chairside object
               :paperFire object
               :samuel object
               :firePlace light
               } mt)
