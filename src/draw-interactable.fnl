(local lg love.graphics)

(local l {:talk-stanley {:x 0 :y -14}
          :talk-samuel {:x 0 :y -8}
          :talk-victoria {:x 0 :y -13}
          :talk-william {:x 0 :y -14}
          :talk-carlington {:x 0 :y -14}
          :talk-alice {:x 0 :y -13}
          :talk-martin {:x 0 :y -14}
          :talk-jane {:x 0 :y -13}
          "paper-fire-evidence" {:x -3 :y -12}
          "knife-block-evidence" {:x 0 :y 0}
          "table-evidence" {:x 8 :y 0}
          "chest-evidence" {:x 0 :y -6}
          "paper-table-evidence-stanley" {:x 0 :y 0}
          "paper-table-evidence-samuel" {:x 0 :y 0}}
       )

(fn update [dt]
  (local interactions (require :interactions))
  (local state (require :state))
  (each [key obj (ipairs state.level.layers.objects.data)]
    ;; (when obj.interaction (pp obj.interaction))
    (when (and obj.interaction (. l obj.interaction.interaction))
      (when (not obj.alert-active)
        (obj:add-alert)
        (set obj.alert-active :yellow)
        )
      (obj:update-alert dt)
      (if (= 0 (interactions.current-questions obj.interaction.interaction))
          (set obj.alert-active :white)
          (set obj.alert-active :yellow))
      ;;
      )
    )  
  )

(fn draw []
  (local state (require :state))
  (each [key obj (ipairs state.level.layers.objects.data)]
    (when obj.alert-active
      (local {: x : y} (. l obj.interaction.interaction))
      ;; (lg.rectangle :fill obj.x obj.y obj.w obj.h)
      ;; (set obj.alert-active :yellow)
      (lg.setColor 1 1 1 1)
      (obj:draw-alert (+ obj.x x) (+ obj.y y))
      )
    )
  )

{: draw : update}
