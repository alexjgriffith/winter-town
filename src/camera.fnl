(fn update-camera [player camera target]
  (match camera.type
    :game (do
            (local offset-x (if player.left -12 0))
            (local offset-y (if player.up -12 0))
            (local ix (math.floor (/ (+ player.col.x 0 offset-x) 16)))
            (local iy (math.floor (/ (+ player.col.y 0 offset-y) 16)))
            (local qx (lume.clamp (math.floor (/ ix 9)) 0 2))
            (local qy (lume.clamp (math.floor (/ iy 7)) 0 3))
            (local tx (- (* qx (- (* 16 10) 16))))
            (local ty (- (* qy (- (* 16 8) 16))))
            (set target.x tx)
            (set target.y ty)
            (set camera.x  ((if (> tx camera.x) math.ceil math.floor)
                            (lume.lerp target.x camera.x
                                       (if target.snap 0 0.9))))
            (set camera.y  ((if (> ty camera.y) math.ceil math.floor)
                            (lume.lerp target.y camera.y
                                       (if target.snap 0 0.9)))))
    :editor (do (set camera.x 0)
                (set camera.y 0))
    )
  (when target.snap
    (set target.snap false))
  )

{: update-camera }
