(local npc {})
(local object-base (require :object))
(setmetatable npc {:__index object-base})
(local lg love.graphics)

(fn npc.draw-shadow [self]
  (local {: image : quad} (. _G.resources :atlas :AtlasShadow))
      (love.graphics.draw image quad self.x (+ 2 self.y)))

(fn npc.draw [self image]
  ;; (local {: image : quad} (. _G.resources :atlas self.sprite))
  (lg.setColor 1 1 1 1)
  ;;(love.graphics.draw image quad self.x self.y)
  (self.animation:draw self.image self.x self.y)
  )

(fn npc.interact [self player]
  (pp :interact)
  (set self.seen true)
  )

(fn npc.update [self dt]
  (local {: update-fire} (require :util))
  (self.animation:update dt)
  (when self.fire (update-fire self dt))
  (local player (. (require :state) :player))
  (if (< player.x self.x)
      (set self.animation.flippedH true)
      (set self.animation.flippedH false))
  )

(local npc-mt {:__index npc})

(local preview-canvas (lg.newCanvas 200 800))
(fn npc.preview [brush]
  (local {: x : y  : name : sprite
          : frames : w : h
          : rate} brush)
    (local anim8 (require :lib.anim8))
    (local {: quad : image } (. resources :atlas sprite))
    
    (lg.setCanvas preview-canvas)
    (lg.setColor 1 1 1 1)
    (lg.clear)
    (lg.draw image quad)
    (lg.setCanvas)
    (local grid (anim8.newGrid w h 200 800))
    (local animation (anim8.newAnimation (grid (.. 1 "-" frames) 1) rate))

  (setmetatable
          {: sprite
           :x x
           :y y 
           : name
           :preview true
           :image preview-canvas
           : animation
           } npc-mt))

(fn npc.create [brush]
  (local {: x : y :  name : sprite
          : interaction : ix : iy : iw : ih
          : frames
          : rate
          : max-radius : min-radius
          : w : h 
          : cx : cy : cw : ch}
         brush)
  (local anim8 (require :lib.anim8))
  (local {: quad : image } (. resources :atlas sprite))
  (local animation-canvas (lg.newCanvas (* frames w) h))
  (lg.setCanvas animation-canvas)
  (lg.setColor 1 1 1 1)
  (lg.draw image quad)
  (lg.setCanvas)
  (local grid (anim8.newGrid w h (* w frames) h))
  (local animation (anim8.newAnimation (grid (.. 1 "-" frames) 1) rate))
  ;;anim8.newGrid(frameWidth, frameHeight, imageWidth, imageHeight
  (local obj
         (setmetatable
          {:brush brush
           : name
           : sprite
           : animation
           :image animation-canvas
           :w w
           :h h
           :x x
           :y y
           :col false
           :type :npc
           } npc-mt))
  (when (and max-radius min-radius)
    (tset obj :fire {:radius (math.random 0 max-radius) :expanding false :max-radius max-radius : min-radius}))
  (local world (. (require :state) :col))
  (when cx
    (set obj.col {:type :col :name name :x (+ x cx)
                  :y (+ y cy) :w cw :h ch})
    (world:add obj.col obj.col.x obj.col.y obj.col.w obj.col.h)
    )
  (when interaction
    (local state (require :state))
    (set obj.interaction {:type :interaction : interaction
                          :name name :x (+ x ix)
                          :y (+ y iy) :w iw :h ih})
    (tset state.mapping.interaction obj.interaction obj)
    (world:add obj.interaction obj.interaction.x obj.interaction.y obj.interaction.w obj.interaction.h))
  obj)

npc
