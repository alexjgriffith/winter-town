(local game {})

(local gamestate (require :lib.gamestate))

(local lg love.graphics)
(local state (require :state))
(local fade (require :lib.fade))

(local draw-interactable (require :draw-interactable))



(when preview (set state.camera.scale 4))

(fn debug-world []
  (local world state.col)
  (local camera state.camera)
  (local (items len) (world:getItems))
  (lg.push)  
  (lg.translate (* camera.scale camera.x) (* camera.scale camera.y))
  (lg.setColor 1 1 1 1)
  (lg.setFont resources.debug-font)
  (for [i 1 len]
    (local {: x : y : w : h : type : interaction} (. items i))
    (match type
      :col (lg.setColor 1 0 0 1)
      :entrance (lg.setColor 0 0 1 1)
      :interaction (lg.setColor 0 0 1 1))
    (love.graphics.rectangle :line
                             (* x camera.scale)
                             (* y camera.scale)
                             (* w camera.scale) (* h camera.scale))
    (match type
      :interaction (do
                     
                     (lg.setColor 0 0 1 1)
                     (lg.setFont resources.button-font)
                     (lg.print interaction (* x camera.scale)
                             (- (* y camera.scale) 16))))
    )
  (lg.pop)  
  )

(fn menu [self]
  (local camera state.camera)
  (lg.rectangle :fill (* 10 16 camera.scale) 0 (* 2 16 camera.scale)
           (* 8 16 camera.scale)))

(fn game.draw [self]
  (local camera state.camera)
  (local draw-interactable (require :draw-interactable))
  (local level state.level)
  (local editor state.level.editor)
  (lg.push)
  (lg.scale camera.scale)
  (lg.translate camera.x camera.y)
  (level:draw-layer :ground)
  (lg.setColor 0.5 0.5 1 0.3)
  (level:operation :draw-shadow :objects)
  (lg.setColor 1 1 1 1)
  (level:draw-layer :objects)
  (draw-interactable.draw)
  (level:draw-layer :cliff)
  (lg.pop)
  (lg.push)
  (lg.scale camera.scale)  
  
  (lg.setColor resources.brown)
  (lg.rectangle :fill (* 10 16) 0 (* 2 16) (* 8 16))
  (lg.setColor 1 1 1 1)
  (level:draw-layer :ui)

  (when (and false preview)
    (lg.setFont resources.title-font)
    (lg.setColor resources.black)
    (lg.printf "Murder of the Marquise" 26 (+ 3 4) 176 :center)
    (lg.setColor resources.white)  
    (lg.printf "Murder of the Marquise" 26 (+ 2 4) 176 :center)
    
    (lg.setFont resources.text-font)
    (lg.setColor resources.black)
    (lg.printf "A Murder mystery" 26 (+ 30 37) 176 :center)
    (lg.setColor resources.white)  
    (lg.printf "A Murder Mystery" 26 (+ 30 36) 176 :center)  
    (lg.setColor 1 1 1 1))
  (lg.draw fade.canvas)
  (lg.pop)
  (when state.editing
    (debug-world)  
    (editor.draw))
  ;; 
  )

(fn game.update [self dt]
  (local camera state.camera)
  (local {: update-camera} (require :camera))
  (local editor state.level.editor)
  (local level state.level)
  (level:update-layer :objects dt)
  (draw-interactable.update dt)
  (when state.fresh
    (set state.target.snap true)
    (state.player:translate 167 164)
    (set state.fresh false))
  (level:update-layer :ground dt)
  (level:update-layer :cliff dt)
  (let [clue (require :clue)] (clue.update))
  (let [chat (require :chat)] (chat.update dt))
  (when (not state.editing) (level:update-layer :ui dt))
  (level:y-sort-layer :objects)
  (state.level.editor.update dt camera)
  (if (and false preview)
      (do (set camera.x state.target.x)
          (set camera.y state.target.y))
    (update-camera state.player camera state.target))
)

(fn game.enter [self dt]
  (local level state.level)
  (fade.in)
  (level:enter-layer :ground dt))

(fn game.leave [self dt]
  (local level state.level)
  (level:leave-layer :ground dt))

(fn game.keypressed [self key code]
  (local editor state.level.editor)
  (local state (require :state))
  (match key
    :escape (do
              (resources.click:stop)
              (resources.click:play)
              (when (= state.ui :chatbox)
                (let [chat (require :chat)] (chat.close))
                (set state.ui :chatbox))
              (set state.ui (if state.ui false :menu)))
    :space (if (= state.ui :chatbox)
               (let [chat (require :chat)] (chat:next))
               (state.player:interact))
    :return (if (= state.ui :chatbox)
                (let [chat (require :chat)] (chat:next))
                (state.player:interact))
    :delete (set state.editing (not state.editing))
    ;; "\\" (state.levels:save)
    _ (if state.editing (state.level.editor.keypressed key code))))

(fn game.mousepressed [self x y button]
  (if state.editing 
      (do (local editor state.level.editor)
          (state.level.editor.mousepressed x y button))
      (do (local level state.level)
          (level:mousepressed-layer :ui x y button)))
  )

(fn game.mousereleased [self x y button]
  (when state.editing
    (local editor state.level.editor)
    (state.level.editor.mousereleased x y button))
  )

(fn love.handlers.gameover [outcome]
  (local state (require :state))
  (set state.outcome outcome)
  (resources.click:stop)
  (resources.click:play)
  (fade.out (fn []
              (gamestate.switch (require :mode-ending))))
  )

game
