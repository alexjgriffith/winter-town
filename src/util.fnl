(fn update-fire [self dt]
  (local fire self.fire)
  (local rate 0.1)
  (if fire.expanding
      (set fire.radius (+ fire.radius (* rate dt)))
      (set fire.radius (- fire.radius (* rate dt)))
      )
  (when (> fire.radius fire.max-radius)
    (set fire.radius fire.max-radius)
    (set fire.expanding false))
  (when (< fire.radius fire.min-radius)
    (set fire.radius fire.min-radius)
    (set fire.expanding true))
  )

(fn check-warp [self]
  (local world (. (require :state) :col))
  (fn filter [item other]
    (when (= other.name :player) :cross))
  (local (_ _ cols len) (world:move self.entrance self.entrance.x self.entrance.y
                                    filter))
  
  (when (> len 0 ) (love.event.push :move-to self.entrance)))

{: update-fire : check-warp}
