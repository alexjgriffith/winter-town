(local house {})
(local object (require :object))
(setmetatable house {:__index object})
(local lg love.graphics)

(fn house.draw [self image]
  (local {: image : quad} (. _G.resources :atlas self.sprite))
  (lg.setColor 1 1 1 1)
  (love.graphics.draw image quad self.x self.y)
  (when self.touched
    (lg.rectangle :fill self.x self.y 20 20))
  )

(fn house.interact [self player]
  (when (or (= 0 self.time) (= self.period self.time))    
    (set self.active (not self.active))    
    (each [key value (ipairs self.linked)]
      (: value (if self.active :link-on :link-off) value))))

(fn house.update [self dt]
  (local {: check-warp} (require :util))
  (check-warp self))

(local house-mt {:__index house})

(fn house.preview [brush]
  (local {: x : y  : name : sprite} brush)
  (setmetatable
          {: sprite
           :x x           
           :y y} house-mt))

(fn house.create [brush]
  (local {: x : y : w : h : name : sprite : cx : cy : cw : ch : ex : ey : ew : eh
          : sx : sy : warp : px : py}
         brush)
  (local obj
         (setmetatable
          {:brush brush
           : name
           : sprite
           :w w
           :h h
           :x x                      
           :y y
           :touched false
           :smoke-stack {:x sx :y sy}
           :type :house
           :linked []
           :col {:type :col :name name :x (+ x cx)
                 :y (+ y cy) :w cw :h ch}
           :entrance {:type :entrance :name name
                      :parent :house
                      : warp
                      : px
                      : py
                      :x (+ x ex)
                      :y (+ y ey) :w ew :h eh}
           } house-mt))
  (local state (require :state))
  (local world (. (require :state) :col))
  (world:add obj.col obj.col.x obj.col.y obj.col.w obj.col.h)
  (world:add obj.entrance obj.entrance.x obj.entrance.y obj.entrance.w obj.entrance.h)
  (tset state.mapping.entrance obj.entrance obj)
  obj)

house


