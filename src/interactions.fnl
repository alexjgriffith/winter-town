(local interactions {})

(local conditions
       {:truth true
        :false false
        :knife-found false
        :knife-block-found false
        :knife-at-marquise false
        :gamble-table-found false
        :stanley-table-found false
        :samuel-table-found false        
        :burnt-debt-papers false
        :jane-dislikes-victoria false
        :met-william false
        :met-victoria false
        :met-carlington false
        :met-alice false
        :met-martin false
        :met-stanley false
        :met-jane false
        :know-jane-was-fired false
        :know-chest-locked false
        :know-victoria-key false
        :has-key false
        :know-chest-contents false
        :knows-victoria-chest-contents false
        :confronted-martin false
        :confronted-stanley false
        :confronted-alice false
        :confronted-jane false
        :confronted-carlington false
        :confronted-victoria false
        :confronted-william false
        :giving-verdict false
        :knows-victoria false
        :knows-william false
        :knows-stanley true
        })


(local paper-fire
       {:name :paper-fire
        :questions
        [{:not [:burnt-debt-papers]
          :set :burnt-debt-papers
          :text "Interesting .."
          :response "Why burn evidence in the open? Each of these houses has a fireplace!"
          :clue "Someone tried to very conspicuously burn debt papers"
          :done false
          }]
        :openers
        [{:not [:burnt-debt-papers]
          :text "It appears someone has tried to burn some debt papers. This seems like a poor spot ..."}
         {:when [:burnt-debt-papers]
          :text "Still burning ..."}
         ]
        :opener ""
        :active-questions []
        :new-questions true
        })

(local samuel
       {:name :samuel
        :questions
        [{:not [:knife-found :knife-block-found]
          :set :knife-found
          :text "Interesting ..."
          :response "He's been dead for an hour."
          :clue "The Marquise has been dead for an hour. The murder weapon seems to have been a knife"
          :done false
          }
         {:not [:knife-found]
          :when [:knife-block-found]
          :set :knife-found
          :text "Interesting ..."
          :response "Well it looks like we know where Jane's knife went. He's been dead for an hour."
          :clue "The Marquise has been dead for an hour. The murder weapon seems to have been a knife"
          :done false
          }]
        :openers
        [{:not [:knife-found]
          :text "Hmmm, there seams to be a knife in this man's back"}
         {:when [:knife-found]
          :text "Still cold ..."}
         ]
        :opener ""
        :active-questions []
        :new-questions true
        })

(local knife-block
       {:name :knife-block
        :questions
        [{:not [:knife-block-found]
          :when [:knife-found]
          :set :knife-block-found
          :text "Interesting ..."
          :response "We'll we know where it is now ..."
          :clue "A knife is missing from Jane's knife block."
          :done false
          }
         {:not [:knife-block-found :knife-found]
          :text "Missing ?"
          :set :knife-block-found
          :response "Lets do a search!"
          :clue "A knife is missing from Jane's knife block."
          :done false
          }]
        :openers
        [{:not [:knife-block-found]
          :text "This knife block is one knife short ..."}
         {:when [:knife-block-found]
          :text "Still missing ..."}
         ]
        :opener ""
        :active-questions []
        :new-questions true
        })


(local gamble-table
       {:name :gamble-table
        :questions
        [{:not [:gamble-table-found :met-stanley]
          :text "Interesting ..."
          :response "A very legitimate gambling den"
          :clue "There seems to be gambling going on at Carl's establishment."
          :done false
          }
         {:not [:gamble-table-found]
          :when [:met-stanley]
          :set :gamble-table-found
          :text "Look Closer"
          :response "You cam see a IOU scribbled out and signed by Stanley."
          :clue "Stanley has been gambling at Carlington's."
          :done false
          }
         ]
        :openers
        [{:not [:gamble-table-found]
          :text "There are cards and chips piled carfully behind the table."}
         {:when [:gamble-table-found]
          :text "The table looks well used."}
         ]
        :opener ""
        :active-questions []
        :new-questions true
        })


(local stanley-table
       {:name :stanley-table
        :questions
        [{:not [:stanley-table-found]
          :set :stanley-table-found
          :text "Interesting ..."
          :response "Stanley is in a LOT of debt."
          :clue "Stanley is in a LOT of debt."
          :done false
          }]
        :openers
        [{:not [:stanley-table-found]
          :text "In the piles of dishevelled papers you can see numerous debt statements."}
         {:when [:stanley-table-found]
          :text "Stanley owes someone a lot of money!"}
         ]
        :opener ""
        :active-questions []
        :new-questions true
        })

(local samuel-table
       {:name :samuel-table
        :questions
        [{:not [:samuel-table-found]
          :set :samuel-table-found
          :text "Interesting ..."
          :response "Someone was blackmailing the Marquise"
          :clue "Someone was blackmailing the Marquise"
          :done false
          }]
        :openers
        [{:not [:samuel-table-found]
          :text "There appear to be regular payments on a miscellaneous account labled BM."}
         {:when [:samuel-table-found]
          :text "Looks like the Marquise has been blackmailed!"}
         ]
        :opener ""
        :active-questions []
        :new-questions true
        })


(local chest
       {:name :chest
        :questions
        [{:not [:chest-found]
          :set :chest-found
          :text "Interesting ..."
          :response "Won't budge"
          :clue "There is a locked chest in Stanley's house."
          :done false
          }
         {:when [:has-key]
          :set :know-chest-contents
          :text "Open!"
          :response "The chest opens to reveal images of the Marquise and Martin indisposed."
          :clue "The Marquise and martin were romantically engaged. Clearly Victoria, William and Stanley knew"
          :done false
          }]
        :openers
        [{:not [:chest-found]
          :text "This chest is locked tight."}
         {:when [:chest-found]
          :not [:has-found :know-chest-contents]
          :text "Still locked."}
         {:when [:know-chest-contents]
          :text "Why did Victoria have the key?"}
         {:when [:has-found]
          :text "The key fits into the lock!"}
         ]
        :opener ""
        :active-questions []
        :new-questions true
        })


(local victoria
       {:name :victoria
        :questions
        [{:when [:accused]
          :text "Case Closed"
          :response ""
          :done false
          }
         {:not [:met-victoria :accused :giving-verdict]
          :set :met-victoria
          :text "Who are you?"
          :response "I am Victoria, the Marchioness!"
          :clue "Victoria was married to the late Marquise"
          :done false
          }
         {:not [:giving-verdict :accused]
          :when [:met-victoria :samuel-table-found]
          :text "Blackmail?"
          :response "My husband would never succumb to blackmail!"
          :done false
          }
         {:not [:giving-verdict :accused]
          :when [:met-victoria :samuel-table-found :knows-victoria-chest-contents]
          :text "Blackmail?"
          :response "It's true, William told me Stanley was blackmailing the marquise to cover his debts!"
          :clue "Stanley was blackmailing the Marquise"
          :done false
          }
         {:not [:giving-verdict :knows-victoria-chest-contents :accused]
          :when [:met-victoria :samuel-table-found :know-chest-contents]
          :text "Chest?"
          :response "This is so embarrassing! I only found out a few weeks ago! William showed me."
          :clue "Victoria knew of her husbands affair, and had been informed by William"
          :done false
          }         
         {:not [:knows-victoria :giving-verdict :accused]
          :set :knows-victoria
          :when [:met-victoria ]
          :text "Body?"
          :response "The Marquise? My Husband! Dead!! How did this happen? Who did this?"
          :done false
          }
         {:when [:knows-victoria :met-stanley]
          :not [:giving-verdict :accused]
          :text :Stanley?
          :response "Stanley? He was found by the body? I don't know him well, perhaps ask William about him."
          :clue "William and Stanley are friends"
          }
         {:when [:knows-victoria :met-martin]
          :not [:giving-verdict :accused]
          :text :Martin?
          :response "Martin? I can't stand the man. He is just so ugh!!"
          :clue "Victoria dislikes martin."
          }
         {:when [:knows-victoria :met-william]
          :not [:giving-verdict :accused]
          :text :William?
          :response "William? Yes, he's my son from my first marriage, with Carlington."
          :clue "William is Victoria's son."
          }
         {:when [:knows-victoria :met-carlington]
          :not [:giving-verdict :accused]
          :text :Carlington?
          :response "Oh Carlington, that does take me back. We where married once you know."
          :clue "Victoria and Carlington were married"
          }
         {:when [:knows-victoria]
          :not [:giving-verdict :accused]
          :text :Where?
          :response "Tonight? I've been out with Alice. William just walked me home."
          }         
         {:when [:knows-victoria :know-jane-was-fired]
          :not [:giving-verdict :accused]
          :text "Jane Fired?"
          :response "My husband and I where going through some hard times. Having her around was NOT helping"
          }         
         {:when [:knows-victoria :know-chest-locked]
          :not [:giving-verdict :know-victoria-key :accused]
          :text "Chest Key?"
          :response "I'm afraid I don't know what you're going on about!"
          :clue "Victoria is acting suspicious about the key to the chest"
          }
         {:when [:knows-victoria :know-victoria-key]
          :not [:giving-verdict :accused]
          :text "Chest Key?"
          :set :has-key
          :response "W who told you? H here it is. It's nothing really ... here it is."
          }
         {:when [:knows-victoria :know-chest-contents]
          :not [:giving-verdict :accused]
          :set :spoken-of-chest
          :text "Chest?"
          :response "Yes, I recently learned. William found out from Stanley. He wanted me to know ..."
          :clue "Victoria knew of her husband's affair. William wanted her to know."
          }
         {:when [:knows-victoria :know-chest-contents :spoken-of-chest]
          :not [:giving-verdict :accused]
          :text "Chest??"
          :response "Don't tell Alice..."
          }         
         {:when [:knows-victoria :knife-at-marquise]
          :not [:giving-verdict :accused]
          :text "Kinfe?"
          :set :mover-william
          :clue "Victoria doesn't know about the missing knife. Maybe William does?"
          :response "Jane's missing a knife? Well it isn't here. Ask William, he helped her move."
          }
         {:when [:knows-victoria]
          :not [:giving-verdict :accused]
          :set :giving-verdict
          :text "I got it!"
          :response "Oh really? So quickly! So who murdered my husband?"
          :always true
          }
         {:when [:giving-verdict :met-victoria]
          :unset :giving-verdict
          :set :accused
          :text "You!"
          :response "How Dare you!"
          :gameover [:lose :victoria]
          :always true
          }
         {:when [:giving-verdict :met-william]
          :unset :giving-verdict
          :set :accused
          :gameover [:win :william]
          :text "Your Son"
          :response "William? He is such an angel, how could he bring himself to do such a thing!"
          :always true
          }
         {:when [:giving-verdict :met-carlington]
          :gameover [:lose :carlington]
          :unset :giving-verdict
          :set :accused
          :text :Carlington
          :response "Carlington? But he and the Marquise got on so well!"
          :always true
          }
         {:when [:giving-verdict :met-alice]
          :gameover [:lose :alice]
          :unset :giving-verdict
          :set :accused
          :text :Alice
          :response "Alice? That poor woman..."
          :always true
          }
         {:when [:giving-verdict :met-martin]
          :gameover [:lose :martin]
          :unset :giving-verdict
          :set :accused
          :text :Martin
          :response "Martin! I should have know, what a snake!"
          :always true
          }
         {:when [:giving-verdict :met-stanley]
          :gameover [:lose :stanley]
          :unset :giving-verdict
          :set :accused
          :text :Stanley
          :response "Stanley? Is he in some sort of trouble?"
          :always true
          }
         {:when [:giving-verdict :met-jane]
          :gameover [:lose :jane]
          :unset :giving-verdict
          :set :accused
          :text :Jane
          :response "Jane? We kept her on for too long. I knew it!"
          :always true
          }         
         {:when [:giving-verdict]
          :unset :giving-verdict
          :text "Nevermind"
          :response "Classic amateurs! Come back when you know"
          :always true
          }         
         ]
        :openers
        [{:not [:met-victoria]
          :text "Another vagabond! Get out of my house!"}
         {:when [:met-victoria]
          :text "Any news on my dearest husband?"}
         ]
        :opener ""
        :active-questions []
        :new-questions true
        })


(local william
       {:name :william
        :questions
        [{:not [:met-william]
          :set :met-william
          :text "Who are you?"
          :response "I'm William."
          :done false
          }
         {:when [:met-william]
          :not [:knows-william]
          :set :knows-william
          :text :Body?
          :response "The Marquise!? Ha! I bet he didn't see that one coming!"
          }
         {:when [:knows-william]
          :text :Where?
          :response "Tonight, I was at my Father's as usual... just walked my mother home."
          :clue "William says he was at his Father's tonight"
          }
         {:when [:met-william]
          :text :Father?
          :clue "William will be the new Marquise"
          :response "Carlington is my father. And, the joke is I'm the Marquise's only heir!"
          }
         {:when [:met-william :burnt-debt-papers]
          :text :Soot?
          :clue "William has soot under his finger nails"
          :response "What soot? There is no soot under my finger nails ..."
          }         
         {:when [:met-william :met-stanley]
          :text :Stanley?
          :response "Stanley? I play cards with him at my Father's. He's a real sucker, my goto fall guy."
          :clue "William thinks Stanley is a chump."
          }
         {:when [:met-william :chest-found]
          :text :Chest?
          :response "Stanley's chest, why are you asking? I don't have the key, my mother does."
          :set :know-victoria-key
          }
         {:when [:knows-william :knife-at-marquise]
          :not [:confronted-william :mover-william]
          :text :Knife?
          :response "Knife, what knife ..."
          :set :confronted-william
          }
         {:when [:knows-william :knife-at-marquise :mover-william]
          :not [:confronted-william]
          :text :Knife?
          :response "I may have seen a knife when moving that old bag. What is it to you?"
          }
         {:when [:knows-william :knife-at-marquise :mover-william :confronted-william]
          :text "Knife Lie?"
          :response "Ok ok, I helped that old bag move, will you lay off about the knife."
          }         
         {:when [:knows-william :knife-at-marquise :confronted-william]
          :not [:mover-william]
          :text :Knife??
          :response "Lay off it already won't you. Go ahead and search, there is no knife here!"
          }
         ]
        :openers
        [{:not [:met-william]
          :text "Mother, do something. A stranger is in the house!"}
         {:when [:met-william]
          :text "... you"}
         ]
        :opener ""
        :active-questions []
        :new-questions true
        })

(local carlington
       {:name :carlington
        :questions
        [{:not [:met-carlington]
          :set :met-carlington
          :text "Who are you?"
          :response "I'm Carlington. I run this fully legitimate establishment."
          :clue "Carl may be doing some shady stuff."
          :done false
          }
         {:not [:knows-carlington]
          :when [:met-carlington]
          :set :knows-carlington
          :text "Body?"
          :response "The Marquise, dead? He was my #1 customer, and covered for my other customers too.."
          :clue "The Marquise was important to Carlington's business"
          :done false
          }
         {:when [:knows-carlington :met-stanley]
          :text "Stanley?"
          :set :spoke-of-stanley
          :response "Stanley? He's been hear all night, just left a couple minutes ago."
          :done false
          }
         {:when [:knows-carlington :met-stanley :gamble-table-found :spoke-of-stanley]
          :text "Stanley?"
          :response "For a man with no luck he sure does love to gamble, but he always pays his debts."
          :done false
          }
         {:when [:knows-carlington :met-william]
          :text "William?"
          :response "William? What can I say, he takes after his old man. A real hustler you know"
          :done false
          }
         {:when [:knows-carlington :know-chest-contents]
          :text "Chest?"
          :response "Ahahaha, I think everyone but their wives knows what's going on there."
          :done false
          }
         {:when [:knows-carlington :burnt-debt-papers]
          :text "Debt?"
          :response "I'm flush, and I don't lend."
          :done false
          }         
         ]
        :openers
        [{:not [:met-carlington]
          :text "You don't look like you have the right cut for this joint kid."}
         {:when [:met-carlington]
          :text "Spend something or get out."}
         ]
        :opener ""
        :active-questions []
        :new-questions true
        })

(local martin
       {:name :martin
        :questions
        [{:not [:met-martin]
          :set :met-martin
          :text "Who are you?"
          :response "I'm Martin, the merchant's husband."
          :done false
          }
         {:when [:met-martin]
          :set :knows-martin
          :text "Body?"
          :response "The Marquise! No, it can't be. I need a minute."
          :clue "Martin seems distraught at the news of the Marquise's death."
          :done false
          }
         {:when [:knows-martin]
          :text :Where?
          :response "Tonight? I've been home all night. With my wife and Victoria."
          }
         {:when [:met-martin :met-stanley]
          :text "Stanley?"
          :response "Yes, Stanley is my son. A good lad, but bad with money."
          :done false
          }
         {:when [:knows-martin :met-william]
          :text "William?"
          :response "William will cheat you for your last pair of socks. He takes advantage of my son."
          :clue "William has previously taken advantage of Stanley"
          :done false
          }
         {:when [:knows-martin :met-carlington]
          :text "Carlington?"
          :response "Don't trust your money at Carlington's. He and his son always clean up"
          :clue "Carlington is fixing games"
          :done false
          }
         {:when [:knows-martin :know-chest-contents]
          :text "Chest?"
          :response "A chest. Oh god! Please don't tell Alice!"
          :done false
          }]
        :openers
        [{:not [:met-martin]
          :text "Hello there!"}
         {:when [:met-martin]
          :text "Any more questions?"}
         ]
        :opener ""
        :active-questions []
        :new-questions true
        })

(local alice
       {:name :alice
        :questions
        [{:not [:met-alice]
          :set :met-alice
          :text "Who are you?"
          :response "I'm Alice, the local merchant. Were you here to buy something?"
          :done false
          }
         {:when [:met-alice]
          :set :knows-alice
          :text "Body?"
          :response "Sweet lord above, rest his soul! The poor Marquis. Have you spoken to his wife?"
          :done false
          }
         {:when [:knows-alice]
          :text :Where?
          :response "Tonight? I've been home all night. With my husband and Victoria."
          :clue "Alice has been home all night with her husband"
          }
         {:when [:met-alice :know-jane-bad-knees]
          :text "Jane Knees?"
          :response "Jane's knees? Everyone knows they are shot. She can barely move in the winter."
          :clue "Alice has confirmed Jane cannot go out in the winter"
          }
         {:when [:knows-alice :met-stanley]
          :text "Stanly?"
          :response "Stanley was found by the body? He's been having money trouble, but this isn't him!"
          :clue "Stan's mom thinks he's been having money troubles"
          :done false
          }
         {:when [:knows-alice :met-victoria]
          :text "Victoria?"
          :response "Victoria was with us all evening. Her son came by an hour ago to walk her home."
          :clue "Victoria has been at the merchants all evening"
          :done false
          }
         {:when [:knows-alice :met-william]
          :text "William?"
          :response "William is an ambitious young man. I don't know where he gets that from."
          :clue "William is ambitious"
          :done false
          }         
         {:when [:met-martin :know-chest-contents]
          :text "Chest?"
          :response "Martin? That cheating bastard! This will be the end of him you mark my words!"
          :clue "Alice didn't know about Martin and the Marquise's affair"
          :done false
          }
         ]
        :openers
        [{:not [:met-alice]
          :text "Hello there!"}
         {:when [:met-alice]
          :text "Any more questions?"}
         ]
        :opener ""
        :active-questions []
        :new-questions true
        })

(local jane
       {:name :jane
        :questions
        [{:not [:met-jane]
          :set :met-jane
          :text "Who are you?"
          :response "I'm Jane. I am the Chef to the Marquise. Or at least I was."
          :clue "Jane used to work for the Marquise."
          :done false
          }
         {:when [:jane-dislikes-victoria]
          :text :Where?
          :set :know-jane-bad-knees
          :response "Tonight? I've been home all night. My old knees make it hard to go out in the snow."
          :clue "Jane says she's been home all night due to bad knees."
          }
         {:when [:knife-block-found :knife-found :met-jane] ;; proxy for knows-jane
          :text "Knife Block?"
          :set :knife-at-marquise
          :response "A missing knife? I think it got lost in the move from the Marquise's kitchen"
          }
         {:when [:met-jane]
          :text "Fired?"
          :response "I was ... let go. Apparently my services were no longer required."
          }
         {:when [:jane-dislikes-victoria :met-stanley]
          :text "Stanley?"
          :response "Stanley was found by the body? I haven't seen him in a while."
          :clue "Jane has not seen Stanley in a while"
          }
         {:when [:jane-dislikes-victoria :met-william]
          :text "William?"
          :set :mover-william
          :response "What a pleasant young lad. He helped me move when his mother fired me you know!"
          :clue "William helped Jane move"
          }         
         {:when [:met-jane :burnt-debt-papers]
          :text "Debt?"
          :response "I have enough put aside to get me through this winter. Don't know about spring."
          :done false
          }
         {:when [:met-jane]
          :text "Body?"
          :set :jane-dislikes-victoria
          :response
          "Oh goodness no! The poor Marquise. I always knew marrying a divorcee would bring him bad luck."
          :clue "The Marquise's wife was previously married."
          :done false
          }
         {:when [:met-jane :jane-dislikes-victoria]
          :text "Wife?"
          :response
          "Victoria! Yes, she was the one who let me go. She's been acting oddly these past few weeks."
          :clue "The Marquise's wife has been acting oddly."
          :done false
          }         
         ]
        :openers
        [{:not [:met-jane]
          :text "H h hello? What are you doing in my house?"}
         {:when [:met-jane]
          :text "Why are you still here?"}
         ]
        :opener ""
        :active-questions []
        :new-questions true
        })

(local stanley
       {:name :stanley
        :questions
        [{:not [:met-stanley]
          :set :met-stanley
          :text "Who are you?"
          :response "The name's Stanley. Some people call me Stan."
          :clue "You found Stan by the scene of the crime"
          :done false
          }
         {:when [:met-stanley]
          :text "Body?"
          :response "It's the Marquise. He was like this when I found him."
          :clue "The dead man is the Marquise."
          :done false
          }
         {:when [:met-stanley]
          :text "Live?"
          :response "I live just over there *he points to the east*"
          :done false
          }
         {:when [:met-stanley :gamble-table-found]
          :text "Carlington's?"
          :response "So what if I like to enjoy myself at Carlington's?"
          :done false
          }
         {:when [:met-stanley :met-victoria]
          :text "Victoria?"
          :response "Victoria? I haven't seen her in a few days. She is going to take this hard."
          :clue "Stan thinks Victoria will take the new's of the Marquise death badly."
          :done false
          }
         {:when [:met-stanley :met-william]
          :text "William?"
          :response "William? I didn't see him tonight at Carlington's."
          :clue "William was not at Carlington's tonight."
          :done false
          }
         {:when [:met-stanley]
          :text "Where?"
          :response "Tonight? I was at Carlington's"
          :done false
          }
         {:when [:met-stanley :burnt-debt-papers]
          :not [:confronted-stanley]
          :text "Debt papers?"
          :response "Debt? No, I don't have a gambling problem."
          :clue "Stan may be a gambler."
          :done false
          }
         {:when [:confronted-stanley :stanley-table-found]
          :text "Debt lies?"
          :response "Ok yes, I've had to borrow some money from the Marquise. But I am not in debt!"
          :clue "Stan has borrowed money, but is not in debt."
          :done false
          }
         {:when [:met-stanley :chest-found]
          :text "Chest?"
          :response "The chest? Yeah, I don't have the key. Talk to William"
          :clue "William may know where the key to the chest is."
          :done false
          }         
         ]
        :openers
        [{:not [:met-stanley]
          :text "This isn't what it looks like!"}
         {:when [:met-stanley]
          :not [:confronted-stanley]
          :text "Oh, it's you ... what do you want?"}
         {:when [:confronted-stanley]
          :text "Leave off me why don't you?"
          }
         ]
        :opener ""
        :active-questions []
        :new-questions true
        })

(fn reduce [x comp comp1]
  
  (if
   (not x) true
   (= (# x) 0) true
   (= (# x) 1) (comp1 (. x 1))
   (= (# x) 2) (and (comp1 (. x 1)) (comp1 (. x 2)))
   (= (# x) 3) (and (comp1 (. x 1)) (comp1 (. x 2)) (comp1 (. x 3)))
   (= (# x) 4) (and (comp1 (. x 1)) (comp1 (. x 2))
                    (comp1 (. x 3)) (comp1 (. x 4)))
   (= (# x) 5) (and (comp1 (. x 1)) (comp1 (. x 2))
                    (comp1 (. x 3)) (comp1 (. x 4))
                    (comp1 (. x 5)))
   (= (# x) 6) (and (comp1 (. x 1)) (comp1 (. x 2))
                    (comp1 (. x 3)) (comp1 (. x 4))
                    (comp1 (. x 5)) (comp1 (. x 6)))
   
  ))

(fn update-opener [who]
  (fn comp1 [a] (. conditions a))
  (fn comp [a b] (and (. conditions a) (. conditions b)))
  (fn comp-or1 [a] (not (comp1 a)))
  (fn comp-or [a b] (and (not (. conditions a)) (not (. conditions b))))  
  (local openers who.openers)
  (each [_ o (ipairs openers)]
    (when (and  (reduce o.not comp-or comp-or1)
                (reduce o.when comp comp1))
      (set who.opener o.text)
      )
    )
  who.opener
  )

(fn update-questions [who]
  (fn comp1 [a] (. conditions a))
  (fn comp [a b] (and (. conditions a) (. conditions b)))
  (fn comp-or1 [a] (not (comp1 a)))
  (fn comp-or [a b] (and (not (. conditions a)) (not (. conditions b))))  
  (local questions who.questions)
  (set who.active-questions [])
  (each [_ q (ipairs questions)]
    (when (and  (reduce q.not comp-or comp-or1)
                (reduce q.when comp comp1)
                (not q.done))
      (table.insert who.active-questions q)))
  who.active-questions
  )

(fn interactions.interact [who]
  ;; (tset conditions :met-stanley true)
  (local state (require :state))
  (update-questions who)
  (update-opener who)
  (set state.chat {:name who.name
                   :text who.opener
                   :questions who.active-questions
                   })
  (set state.ui :chatbox)
  )


(local name-table
       {: stanley
        : samuel
        : victoria
        : william
        : carlington
        : alice
        : martin
        : jane
        : paper-fire
        : knife-block
        : gamble-table
        : chest
        : stanley-table
        : samuel-table
        })

(fn interactions.question [who-name question]
  (local state (require :state))  
  (local {:set set-condition : response : unset} question)
  (local who (. name-table  who-name))
  (when (not question.always)
    (set question.done true))
  (when set-condition
    (tset conditions set-condition true))
  (when unset
    (tset conditions unset false))
  (when question.clue
    (table.insert state.clues question.clue))
  (each [name w (pairs name-table)]
    (update-questions w))
  (update-opener who)
  (pp question)
  (set state.chat {:name who.name
                   :text response
                   :questions who.active-questions
                   :callback (when question.gameover
                               (fn [] (love.event.push :gameover question.gameover)))
                   })
  (set state.ui :chatbox)
  )


(each [name w (pairs name-table)]
  (update-questions w))

(fn love.handlers.interaction [{: interaction}]
  (resources.click:stop)
  (resources.click:play)
  (match interaction
    "talk-stanley" (interactions.interact stanley)
    "talk-samuel" (interactions.interact samuel)
    "talk-victoria" (interactions.interact victoria)
    "talk-william" (interactions.interact william)
    "talk-carlington" (interactions.interact carlington)
    "talk-alice" (interactions.interact alice)
    "talk-martin" (interactions.interact martin)
    "talk-jane" (interactions.interact jane)
    "paper-fire-evidence" (interactions.interact paper-fire)
    "knife-block-evidence" (interactions.interact knife-block)
    "table-evidence" (interactions.interact gamble-table)
    "chest-evidence" (interactions.interact chest)
    "paper-table-evidence-stanley" (interactions.interact stanley-table)
    "paper-table-evidence-samuel" (interactions.interact samuel-table)))


(local l {:talk-stanley :stanley
          :talk-samuel :samuel
          :talk-victoria :victoria
          :talk-william :william
          :talk-carlington :carlington
          :talk-alice :alice
          :talk-martin :martin
          :talk-jane :jane
          "paper-fire-evidence" :paper-fire
          "knife-block-evidence" :knife-block
          "table-evidence" :gamble-table
          "chest-evidence" :chest
          "paper-table-evidence-stanley" :stanley-table
          "paper-table-evidence-samuel" :samuel-table}
       )


(fn interactions.current-questions [name]
  (# (. name-table (. l name) :active-questions))
  )

interactions
