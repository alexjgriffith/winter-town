(local menu {})

(local gamestate (require :lib.gamestate))

(local fade (require :lib.fade))
(var click-down false)
(var hovered false)
(local lg love.graphics)
(var hover-reset true)

(fn reset []
  (local state (require :state))
  (local player (. state :player))
  (set state.target.snap true)
  (tset state :ui nil)
  (tset state :message nil)
  (tset state :clues [])
  (state.levels:set-level :map 167 164)
  (set state.fresh true)
  (lume.hotswap  :interactions))


(fn click [text dx]
  (local down (and (= 0 dx) (love.mouse.isDown 1)))
  (if down
      (when (not click-down)
        (resources.click:stop)
        (resources.click:play)
        (match text
          :Back (fade.out (fn []
                            (reset)
                            (gamestate.switch (require :mode-menu))))
          )
        (set click-down true)
        )
      (do
        (when (and (~= text hovered) (= 0 dx))
          (do (set hovered text)
              (resources.hover:stop)
              (resources.hover:play)))
        (set click-down false))))


(fn draw-small-button [text mx my x y image upquad downquad iconquad dx]
  (local (bw bh) (values 10 10))
  (lg.setColor 1 0 0 1)
  (lg.setColor 1 1 1 1)
  (if (pointWithin mx my (+ x 2) (+ y 2) bw bh)
      (do (lg.draw image upquad x y)
          (click text dx)
          (set hover-reset false)
          (lg.draw image iconquad (+ x 4) (+ y 4)))
      (do (lg.draw image downquad x y)
          (lg.draw image iconquad (+ x 4) (+ y 3)))))

(fn menu.enter [self]
  (fade.in))

(local ending-text
       {:william "William was the murderer. He was a power hungry young man who killed the Marquise in cold blood.

Unfortunately I think the Marchioness may keep this hushed up.

I'd recomend skipping town. The new Marquise may want a word with you."
        :victoria "Victoria did not murder her husband, and she will not appreciate you spreading lies.

She has started spreading rumours of your coincidental arrival already. You had best skip town."
        :stanley "Stanley has been set up! He was using the Marquise to pay his debts to Carlington.

He was just a poor man at the wrong place at the wrong time.

I'm sure the Marchioness will see him hang."
        :carlington "Carlington may run some shady hustles, but those hustles don't involve murder.

The old Marquise helped cover the debts of those who lost at his establishment, I doubt the new Marquise will do the same."
        :alice "Oh Alice, she's had a tough life. But, she did not kill the Marquise.

Victoria knows this, as she spent the evening of the murder with her.
"
        :martin "Oh Martin, this is NOT his day. He did not kill his lover, the Marquise, but Victoria will make sure he hangs for it.

She has sent you on your way with a tidy sum of cash for your troubles. 
"
        :jane "Jane was an easy scapegoat ... too easy. She did not kill the Marquise.

But, Victoria is happy to just have someone to blame for the death of her late husband."})

(fn menu.draw []
  (local dx 0)
  (local (px py) (love.mouse.getPosition))
  (local (mx my) (values (/ px 5) (/ py 5)))
  (local {:quad blankQuad} (. resources.atlas :UIBlank))
  (local {:quad downSmallQuad} (. resources.atlas :UIButtonSmallUp))
  (local {:quad upSmallQuad} (. resources.atlas :UIButtonSmallDown))
  (local {: image :quad xQuad} (. resources.atlas :UIX))
  (set hover-reset true)
  (lg.push)
  (lg.scale 5)
  (lg.setColor resources.black)
  (lg.rectangle :fill 0 0 200 200)
  (lg.setColor 1 1 1 1)
  (lg.draw image blankQuad)
  (lg.setColor resources.black)
  (lg.setFont resources.text-font)
  (local state (require :state))
  (lg.printf (. ending-text (. state.outcome 2)) 34 9 110)
  (lg.setColor 1 1 1 1)
  (draw-small-button :Back mx my (+ 48 dx 64 16) 106 image upSmallQuad downSmallQuad xQuad dx)
  (lg.draw fade.canvas)
  (lg.pop)
  (when hover-reset
    (set hovered false))
  )

(fn menu.keypressed [self key code]
  (match code
    :return (do
             (resources.click:stop)
             (resources.click:play)
             (fade.out (fn []
                         (reset)
                         (gamestate.switch (require :mode-menu))))
             )
    :escape (do
             (resources.click:stop)
             (resources.click:play)
             (fade.out (fn []
                         (reset)
                         (gamestate.switch (require :mode-menu))))
             )    
    :space (do
             (resources.click:stop)
             (resources.click:play)
             (fade.out (fn []
                         (reset)
                         (gamestate.switch (require :mode-menu))))
          )
             ))


menu
