(local slider {})
(local object-base (require :object))
(setmetatable slider {:__index object-base})
(local lg love.graphics)

(local flux (require :lib.flux))

(fn slider.mousepressed [self x y button]
  (local state (require :state))
  (when self.hover
    (resources.click:stop)
    (resources.click:play)
    (if (= self.name state.ui)
        (set state.ui false)
        (set state.ui self.name))))

(fn slider.draw [self image]
  ;; (pp self)
  (local camera (. (require :state) :camera))
  (local {: image : quad} (. resources :atlas self.sprite))
  (lg.setColor 1 1 1 1)
  (love.graphics.draw image quad (+ self.x self.dx) self.y)
  ;; (lg.rectangle :line (+ self.dx self.col.x) self.col.y self.col.w self.col.h)
  (lg.setFont resources.button-font)
  (lg.setColor resources.black)
  (local (px py) (love.mouse.getPosition))
  (local (mx my) (values (/ px camera.scale) (/ py camera.scale)))
  (local within (pointWithin mx my (+ self.dx self.col.x) self.col.y self.col.w self.col.h))
  (if (and (not self.hover) within)
      (do
        (set self.hover true)
        (when (or (= self.dx 0) (= self.dx self.dx-max))
          (resources.hover:stop)
          (resources.hover:play))
        (set self.dx-target (- self.dx-max 16))
        (when (not self.active) (flux.to self 1 { :dx self.dx-target}))
        )
      (and self.hover (not within))
      (do (set self.hover false)
          (set self.dx-target self.dx-max)
          (when self.tween (self.tween:stop))
          (when (not self.active) (flux.to self 1 { :dx self.dx-max}))))
  (when self.hover
    (lg.setColor resources.grey))
  (lg.print self.brush.text (+ self.col.x  self.dx) self.col.y (/ math.pi (/ 2  3))
            1 1 36 -3)
  (lg.setColor 1 1  1 1)
  (local state (require :state))
  (match self.brush.text
    :MENU (let [menu (require :menu)] (when self.active (menu.draw mx my self.dx)))
    :CLUE (let [clue (require :clue)] (when self.active (clue.draw mx my self.dx)))
    :CHAT (let [chat (require :chat)] (when (and state.chat self.active) (chat.draw mx my self.dx))))
  )

(fn slider.interact [self player])

(fn slider.update [self dt]
  (local state (require :state))
  (if (and (not self.active ) (= self.name state.ui))
      (do (set self.active true)
          (set self.col.w self.cw-min)
           (flux.to self 1 {:dx 0}))
       (and  self.active (~= self.name state.ui))
       (do (set self.active false)
           (set self.col.w self.cw-default)
           (flux.to self 1 {:dx self.dx-max})))
  (flux.update dt)
  )

(local slider-mt {:__index slider})

(fn slider.preview [brush]
  (local {: x : y  : name : sprite} brush)
  (setmetatable
          {: sprite
           :x x           
           :y y} slider-mt))

(fn slider.create [brush]
  (local {: x : y :  name : sprite
          : cx : cy : cw : ch
          : w : h
          : onclick
          : dx-max}
         brush)
  (local obj
         (setmetatable
          {:brush brush
           : name
           : sprite
           :w w
           :tween false
           :active false
           :hover false
           :h h
           :x x
           :cw-default cw
           :cw-min 16
           :dx-max dx-max
           :dx dx-max
           :dx-target dx-max
           :y y
           :col {:x cx :y cy :w cw :h ch}
           :onclick onclick
           :type :slider
           } slider-mt))
  obj)

slider
