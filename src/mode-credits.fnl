(local credits {})
(local gamestate (require :lib.gamestate))
(local fade (require :lib.fade))
(local resources (require :resources))

(local lg love.graphics)


(fn credits.enter [self from]
  (fade.in)
  (set self.from from))

(var click-down false)
(var hovered false)
(fn click [self text]
  (local down (love.mouse.isDown 1))
  (if down
      (when (not click-down)
        (resources.click:stop)
        (resources.click:play)
        (match text
          :Back (fade.out (fn [] (gamestate.switch self.from)
                            (local state (require :state))
                            (set state.ui false)))
          )
        (set click-down true)
        )
      (do
        (when (~= text hovered)
          (do (set hovered text)
              (resources.hover:stop)
              (resources.hover:play)))
        (set click-down false)))
  )


(local credits1 "
Game Code - Alexander Griffith
Other Art - Alexander Griffith
Music - Alexander Griffith
SFX - Alexander Griffith
Font - FreePixel
Cursor - Kemono
Library (Bump,Anim8) - Kikito
Library (Lume,Flux) - RXI
Engine (LOVE) - LOVE Dev Team
Fennel - Calvin Rose
Web Support - Davidobot")

(fn credits.draw [self]
  (lg.push)
  (lg.clear resources.white)
  (lg.setColor resources.black)
  (lg.scale 5)
  (lg.setFont resources.button-font)
  (lg.printf "Credits" 0 2 176 :center)
  (lg.setFont resources.text-font)
  (lg.printf credits1 2 10 176 :left)
  (lg.setColor 1 1 1 1)
  (local (x y) (love.mouse.getPosition))
  (local (mx my) (values (/ x 5) (/ y 5)))
  (local (bw bh) (values (* 48 1) (* 16 1)))
  (local {:quad lsQuad : image} resources.atlas.UIButtonSmallDown)
  (local {:quad dsQuad } resources.atlas.UIButtonSmallUp)
  (local {:quad xQuad} resources.atlas.UIX)
  (let [x 140 y 100]
    (if (pointWithin mx my x y 16 16)
      (do (lg.draw image lsQuad x y)
          (lg.draw image xQuad (+ 4 x) (+ 4 y))
          (click self :Back))
      (do (lg.draw image dsQuad x y)
          (set hovered false)
          (lg.draw image xQuad (+ 4 x) (+ 3 y)))))
  (lg.draw fade.canvas)
  (lg.pop)
  )

(fn credits.update [self])

(fn credits.keypressed [self key code]
  (match key
    :escape (fade.out (fn [] (gamestate.switch self.from)
                        (local state (require :state))
                            (set state.ui false)))))

credits
