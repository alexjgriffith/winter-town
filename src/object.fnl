(local object {})

(local lg love.graphics)

(fn object.remove [self]
  (local world (. (require :state) :col))  
  (fn filter [obj]
    (fn [item]    
      (= (fennel.view item) (fennel.view obj))))
  (when self.col
    (if (world:hasItem self.col)
        (world:remove self.col)
        (let [(items len) (world:queryRect -100 -100 2000 2000
                                           (filter self.col))]
          (when (> len 0)
            (world:remove (. items 1))))))

  (when self.cols
    (each [_ col (ipairs self.cols)]
      (if (world:hasItem col)
          (world:remove col)
          (let [(items len) (world:queryRect -100 -100 2000 2000
                                             (filter col))]
            (when (> len 0)
              (world:remove (. items 1)))))))

  (when self.entrance
    (if (world:hasItem self.entrance)
      (world:remove self.entrance)
      (let [(items len) (world:queryRect -100 -100 2000 2000
                                         (filter self.entrance))]
        (when (> len 0)
          (world:remove (. items 1))))))
  )

(fn object.debug-draw [self camera]
  (local {: x : y : w : h} self)
  (local scale camera.scale)
  (love.graphics.push)
  (lg.translate (* camera.scale camera.x) (* camera.scale camera.y))
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.rectangle :line
                           (* scale x)
                           (* scale y)
                           (* scale w)
                           (* scale h))
  (love.graphics.pop))

(fn object.serialize [self] self.brush)

(fn object.add-alert [self]
  (local anim8 (require :lib.anim8))
  (local {:quad alert-quad : image} (. resources :atlas :UIAlert))
  (local {:quad alert-quad-white} (. resources :atlas :UIAlertWhite))
  (local alert-canvas (lg.newCanvas 32 32))
  (lg.setCanvas alert-canvas)
  (lg.setColor 1 1 1 1)
  (lg.draw image alert-quad)
  (lg.draw image alert-quad-white 0 16)
  (lg.setCanvas)
  (local alert-grid (anim8.newGrid 16 16 32 32))
  (local rate (+ 0.3 (/ (math.random) 4)))
  (local alert-yellow (anim8.newAnimation (alert-grid (.. 1 "-" 2) 1) rate))
  (local alert-white (anim8.newAnimation (alert-grid (.. 1 "-" 2) 2) rate))
  (set self.alert-image alert-canvas)
  (set self.alert-yellow alert-yellow)
  (set self.alert-white alert-white)
  (set self.alert-active false)
  )

(fn object.draw-alert [self x y]
  (match self.alert-active
    :yellow (self.alert-yellow:draw self.alert-image x y)
    :white (self.alert-white:draw self.alert-image x y))
  )

(fn object.update-alert [self dt]
  (match self.alert-active
    :yellow (self.alert-yellow:update dt)
    :white (self.alert-white:update dt))
  )

object
