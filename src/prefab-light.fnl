(local light {})
(local object-base (require :object))
(setmetatable light {:__index object-base})
(local lg love.graphics)

(fn light.draw [self image]
  ;; (local {: image : quad} (. _G.resources :atlas self.sprite))
  (lg.setColor 1 1 1 1)
  ;;(love.graphics.draw image quad self.x self.y)
  (self.animation:draw self.image self.x self.y)
  )

(fn light.interact [self player])

(fn light.update [self dt]
  (local {: update-fire} (require :util))
  (self.animation:update dt)
  (update-fire self dt))

(local light-mt {:__index light})

(local preview-canvas (lg.newCanvas 200 800))
(fn light.preview [brush]
  (local {: x : y  : name : sprite
          : frames : w : h
          : rate} brush)
    (local anim8 (require :lib.anim8))
    (local {: quad : image } (. resources :atlas sprite))
    
    (lg.setCanvas preview-canvas)
    (lg.setColor 1 1 1 1)
    (lg.clear)
    (lg.draw image quad)
    (lg.setCanvas)
    (local grid (anim8.newGrid w h 200 800))
    (local animation (anim8.newAnimation (grid (.. 1 "-" frames) 1) rate))

  (setmetatable
          {: sprite
           :x x
           :y y
           : name
           :preview true
           :image preview-canvas
           : animation
           } light-mt))

(fn light.create [brush]
  (local {: x : y :  name : sprite
          : frames
          : rate
          : w : h : max-radius : min-radius
          : cx : cy : cw : ch}
         brush)
  (local anim8 (require :lib.anim8))
  (local {: quad : image } (. resources :atlas sprite))
  (local animation-canvas (lg.newCanvas (* frames w) h))
  (lg.setCanvas animation-canvas)
  (lg.setColor 1 1 1 1)
  (lg.draw image quad)
  (lg.setCanvas)
  (local grid (anim8.newGrid w h (* w frames) h))
  (local animation (anim8.newAnimation (grid (.. 1 "-" frames) 1) rate))
  ;;anim8.newGrid(frameWidth, frameHeight, imageWidth, imageHeight
  (local obj
         (setmetatable
          {:brush brush
           : name
           : sprite
           : animation
           :image animation-canvas
           :w w
           :h h
           :x x
           :y y
           :col false
           :fire {:radius (* max-radius (math.random )) :expanding false :max-radius max-radius : min-radius}
           :type :light
           } light-mt))
  (local world (. (require :state) :col))
  (when cx
    (set obj.col {:type :col :name name :x (+ x cx)
                  :y (+ y cy) :w cw :h ch})
    (world:add obj.col obj.col.x obj.col.y obj.col.w obj.col.h)
    )
  obj)

light
