(local highlayer {})
(local maplayer (require :prefab-maplayer))
(setmetatable highlayer {:__index maplayer})

(fn highlayer.update [self dt]
  (local {: check-warp} (require :util))
  (check-warp self))

(fn highlayer.interact [self player])

(local highlayer-mt {:__index highlayer})

(fn highlayer.create [brush]
  (local world (. (require :state) :col))
  (local {: x : y :  name : sprite
          : warp : ex : ey : ew : eh
          } brush)
  (local obj
         (setmetatable
          {:brush brush
           : name
           : sprite
           :w 448
           :h 464
           :x x
           :y y
           :cols []
           :entrance nil
           :type :highlayer
           } highlayer-mt))
  (when warp
    (set obj.entrance {:type :entrance :name name
                       : warp
                       :parent :highlayer
                       :x (+ x ex)
                       :y (+ y ey) :w ew :h eh})
    (local state (require :state))
    (tset state.mapping.entrance obj.entrance obj)
    (world:add obj.entrance obj.entrance.x obj.entrance.y obj.entrance.w obj.entrance.h))
  (for [i 1 20]
    (when (. brush (.. :cx i))
      (let [cx (. brush (.. :cx i))
            cy (. brush (.. :cy i))
            cw (. brush (.. :cw i))
            ch (. brush (.. :ch i))
            col {:type :col :name name :index i :x (+ x cx)
                 :y (+ y cy) :w cw :h ch}]
        (table.insert obj.cols col)
        (world:add col col.x col.y col.w col.h)
        )
      )
    )
  
  obj)

highlayer
