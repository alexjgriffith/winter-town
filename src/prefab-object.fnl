(local object {})
(local object-base (require :object))
(setmetatable object {:__index object-base})
(local lg love.graphics)

(fn object.draw-shadow [self]
  (when (= :samuel self.name)
    (local {: image : quad} (. _G.resources :atlas :AtlasShadow))
    (lg.push :all)
    (lg.setColor 1 0 0 0.3)
    (love.graphics.draw image quad self.x (+ 2 self.y))
    (lg.pop)))

(fn object.draw [self image]
  (local {: image : quad} (. resources :atlas self.sprite))
  (lg.setColor 1 1 1 1)
  (if self.image
      (self.animation:draw self.image self.x self.y)
      (love.graphics.draw image quad self.x self.y)))

(fn object.interact [self player]
  (set self.seen true))

(fn object.update [self dt]
  (local {: update-fire} (require :util))
  (when self.fire (update-fire self dt))
  (when self.animation (self.animation:update dt)))

(local object-mt {:__index object})

(fn object.preview [brush]
  (local {: x : y  : name : sprite} brush)
  (setmetatable
          {: sprite
           :x x           
           :y y} object-mt))

(fn object.create [brush]
  (local {: x : y :  name : sprite
          : cx : cy : cw : ch
          : w : h
          : frames : rate
          : max-radius : min-radius
          : interaction : ix : iy : iw : ih}
         brush)
  (local obj
         (setmetatable
          {:brush brush
           : name
           : sprite
           :w w
           :h h
           :x x
           :y y
           :col false
           :interaction false
           :type :object
           } object-mt))
  (when (and max-radius min-radius)
    (tset obj :fire {:radius (math.random 0 max-radius) :expanding false :max-radius max-radius : min-radius}))
  (when frames
    (local anim8 (require :lib.anim8))
    (local {: quad : image } (. resources :atlas sprite))
    (local animation-canvas (lg.newCanvas (* frames w) h))
    (lg.setCanvas animation-canvas)
    (lg.setColor 1 1 1 1)
    (lg.draw image quad)
    (lg.setCanvas)
    (local grid (anim8.newGrid w h (* w frames) h))
    (local animation (anim8.newAnimation (grid (.. 1 "-" frames) 1) rate))
    (set obj.animation animation)
    (set obj.image animation-canvas)
    )
  
  (local world (. (require :state) :col))
  (when cx
    (set obj.col {:type :col :name name :x (+ x cx)
                  :y (+ y cy) :w cw :h ch})
    (world:add obj.col obj.col.x obj.col.y obj.col.w obj.col.h)
    )
  (when interaction
    (local state (require :state))
    (set obj.interaction {:type :interaction : interaction
                          :name name :x (+ x ix)
                          :y (+ y iy) :w iw :h ih})
    (tset state.mapping.interaction obj.interaction obj)
    (world:add obj.interaction obj.interaction.x obj.interaction.y obj.interaction.w obj.interaction.h))
  obj)

object
