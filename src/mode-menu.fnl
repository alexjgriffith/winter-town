(local menu {})

(local resources (require :resources))
(local lg love.graphics)

(local fade (require :lib.fade))
(local gamestate (require :lib.gamestate))
(local flux (require :lib.flux))

(local state (require :state))

(local alpha {:value 0})

(local camera {:x -20 :y -20 :scale 5 :type :game})

(when preview (set camera.scale 4))

(fn menu.leave [self])

(local tweens {})
(local ease :linear)

(fn tweens.tween1 []
  (local tween (flux.to camera 16 {:x -160 :y -160}))
  (tween:ease ease)
  (tween:oncomplete tweens.tween2))

(fn tweens.tween2 []
  (local tween (flux.to camera 8 {:x -40  :y -160}))
  (tween:ease ease)
  (tween:oncomplete tweens.tween3))

(fn tweens.tween3 []
  (local tween (flux.to camera 12 {:x -20  :y -20}))
  (tween:ease ease)
  (tween:oncomplete tweens.tween1))

(fn menu.init [self dt]
  (local tween (flux.to alpha 3 {:value 1}))
  (tween:ease :quadinout)
  (tween:delay 2)
  (tweens.tween1)
  (fade.in)
  )

(var click-down false)
(var hovered false)
(var hover-reset true)

(fn click [text dx]
  (local down (and (= 0 dx) (love.mouse.isDown 1)))
  (if down
      (when (not click-down)
        (resources.click:stop)
        (resources.click:play)
        (match text
          :Play (fade.out (fn []                             
                            (gamestate.switch (require :mode-intro))))
          )
        (set click-down true)
        )
      (do
        (when (and (~= text hovered) (= 0 dx))
          (do (set hovered text)
              (resources.hover:stop)
              (resources.hover:play)))
        (set click-down false))))

(fn menu.draw [self]
  (local level state.level)
  (local editor state.level.editor)
  (lg.push)
  (lg.scale camera.scale)
  (lg.translate camera.x camera.y)
  (level:draw-layer :ground)
  (lg.setColor 0.5 0.5 1 0.3)
  (level:operation :draw-shadow :objects)
  (lg.setColor 1 1 1 1)
  (level:draw-layer :objects)
  (level:draw-layer :cliff)
  (local frame-width (if preview (/ 630 camera.scale) 176))
  (lg.pop)
  (lg.push)
  (lg.scale camera.scale)
  
  
  (lg.setFont resources.title-font)
  (lg.setColor resources.black)
  (lg.printf "Murder of the Marquies" 0 3 frame-width :center)
  (lg.setColor resources.white)  
  (lg.printf "Murder of the Marquies" 0 2 frame-width :center)

  (lg.setFont resources.text-font)
  (lg.setColor resources.black)
  (lg.printf "A Murder mystery" 0 37 frame-width :center)
  (lg.setColor resources.white)  
  (lg.printf "A Murder Mystery" 0 36 frame-width :center)  
  

  (local (px py) (love.mouse.getPosition))
  (local (mx my) (values (/ px camera.scale) (/ py camera.scale)))
  (set hover-reset true)
  (var color resources.white)
  (var dy 0)
  (if (and (pointWithin mx my (- 88 15) 61 30 16) (> alpha.value 0.3))
      (do
        (set hover-reset false)
        (click :Play 0)
        (set dy 1)
        )
      (do :nothing))
  (when hover-reset (set hovered false))
  (when (not preview)
    (let [[r g b] resources.black]
      (lg.setColor r g b alpha.value))
    (lg.rectangle :line (- 88 15) 61 30 16 0)  
    (let [[r g b] resources.white]
      (lg.setColor r g b alpha.value))
    (lg.rectangle :line (- 88 15) (+ dy 60) 30 16 0)  
    (lg.setFont resources.text-font)
    (let [[r g b] resources.black]
      (lg.setColor r g b alpha.value))
    (lg.printf "Play" 0 (+ dy 65) frame-width :center)
    (let [[r g b] color]
      (lg.setColor r g b alpha.value))
    (lg.printf "Play" 0 (+ dy 64) frame-width :center)  
    (lg.setColor 1 1 1 1)
    (lg.draw fade.canvas)
    )
  (lg.setFont resources.text-font)
  (lg.setColor resources.black)
  (lg.printf "@gamesbygriffith" 2 (if preview 116 119) frame-width :left)
  (lg.setColor resources.white)  
  (lg.printf "@gamesbygriffith" 2 (if preview 115 118) frame-width :left)  
  (lg.setColor 1 1 1 1)
  (lg.draw fade.canvas)
  
  (lg.pop)
  
  )

(fn menu.update [self dt]
  (local level state.level)
  (level:update-layer :objects dt :menu)
  (level:update-layer :ground dt)
  (level:update-layer :cliff dt)
  (level:y-sort-layer :objects)
  (when (not preview) (flux.update dt))
  (when preview
    (set camera.x -120)
    (set camera.y -60))
  )

(fn menu.keypressed [self key code]
  (match code
    :return (do
             (resources.click:stop)
             (resources.click:play)
             (fade.out (fn []                             
                         (gamestate.switch (require :mode-intro))))
             )
    :space (do
             (resources.click:stop)
             (resources.click:play)
             (fade.out (fn []                             
                         (gamestate.switch (require :mode-intro))))
             )
    :escape (do
              (resources.click:stop)
              (resources.click:play)
              (fade.out (fn []                             
                         (gamestate.switch (require :mode-intro))))
              )
    ;; :delete (set preview (not preview))
    
             ))

menu
