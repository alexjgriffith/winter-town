(local atlas :/assets/sprites/map.png)
(local level (require :lib.editor))
(local state (require :state))
(local bump (require :lib.bump))
(local objects (require :objects))
(local object-creation-callbacks (objects:get-create))
(local object-preview-callbacks (objects:get-preview))

(fn remove-object [name obj]
  ((. objects name :remove) obj))

(fn love.handlers.edit-level [key layer-name brush-name x y tilesize?]
  (match key
    :replace-tile :nil
    :add-object :nil
    :remove-object (remove-object brush-name x)
    ))

(fn rebrush [map-data layer-name]
  (local layer (. map-data :layers layer-name))
  (local data (. layer :data))
  (local brushes (. layer :brushes))
  (local callbacks (. map-data :object-creation-callbacks))
  (var new-data [])
  (each [i d (ipairs data)]
    (local {: x : y : name} d)
    (local brush (lume.clone (. brushes name)))
    (set brush.x x)
    (set brush.y y)
    (tset new-data i ((. callbacks name) brush))
    )
  (set layer.data new-data)
  map-data)

(local editor level.editor)
(local brush editor.brush)

(tset brush :brushes  [[:objects :light :object]
                       ;; [:objects :fire :object]
                       [:objects :house4 :object]
                       [:objects :firePlace :object]
                       [:objects :tree :object]
                       [:objects :bed :object]
                       [:objects :doublebed :object]
                       [:objects :table :object]
                       [:objects :counter :object]
                       [:objects :curtain1 :object]
                       [:objects :curtain2 :object]
                       [:objects :curtain3 :object]
                       [:objects :carlington :object]
                       [:objects :victoria :object]
                       [:objects :william :object]
                       [:objects :alice :object]
                       [:objects :martin :object]
                       [:objects :stanley :object]
                       [:objects :jane :object]
                       [:objects :samuel :object]
                       [:objects :knifeblock :object]                       
                       [:objects :paperFire :object]
                       [:objects :stanleyTable :object]
                       [:objects :samuelTable :object]
                       [:objects :chairside :object]
                       [:objects :chairfront :object]
                       [:objects :chest :object]
                       ;; [:objects :house1 :object]
                       ;; [:objects :house2 :object]
                       ;; [:objects :house3 :object]
                       ;; [:objects :house4 :object]
                       ;; [:objects :house5 :object]
                       ])

(tset brush :count (# brush.brushes))

(tset brush :index 1)

(fn editor.keypressed [key code]
  (match key
    "[" (brush.decrement)
    "]" (brush.increment)))

(local ground-brushes {:ground {:name :ground :sprite :BGMap}
                       :road {:name :road :sprite :RoadMap}
                       :lowcliff {:name :lowcliff :sprite :CliffsMap}
                       :room1 {:name :room1 :sprite :RoomsInteriorHouse1}
                       :room2 {:name :room2 :sprite :RoomsInteriorHouse2}
                       :room3 {:name :room3 :sprite :RoomsInteriorHouse3}
                       :room4 {:name :room4 :sprite :RoomsInteriorHouse4}
                       :room5 {:name :room5 :sprite :RoomsInteriorHouse5}
                       :footsteps {:name :footsteps
                                   :sprite-right :AtlasFootRight
                                   :sprite-left :AtlasFootLeft
                                   :stride 8
                                   :max 20}
                       :firesound {:name :firesound
                                   :sound-name :fire
                                   :proximity 20
                                   :target :fire
                                   :max-volume 0.1
                                   :roll-off 60}
                       })

(local cliff-brushes {:highcliff {:name :highcliff :sprite :CliffsHighMap
                                  :cx1 0 :cy1 0 :cw1 448 :ch1 64
                                  :cx2 0 :cy2 64 :cw2 32 :ch2 384
                                  :cx3 416 :cy3 64 :cw3 32 :ch3 384
                                  :cx4 0 :cy4 448 :cw4 448 :ch4 16
                                  :ex 0 :ey 0 :ew 16 :eh 16
                                  :warp :nil
                                  }
                      :highroom1
                      {:name :highroom1
                       :sprite :UpperRoomsInteriorHouse1
                       :cx1 0 :cy1 0 :cw1 304 :ch1 48 ;; top
                       :cx2 0 :cy2 0 :cw2 16 :ch2 240 ;; left
                       :cx3 288 :cy3 0 :cw3 16 :ch3 240 ;; right
                       :cx4 140 :cy4 0 :cw4 24 :ch4 64 ;; mid
                       :cx5 0 :cy5 (- 128 24) :cw5 43 :ch5 24 ;; bottomleft
                       :cx6 43 :cy6 (- 128 16) :cw6 24 :ch6 16 ;; bottomdoor
                       :cx7 (+ 43 24) :cy7 (- 128 24) :cw7 (- 304 43 24) :ch7 24 ;; bottomright
                       :cx8 140 :cy8 (- 128 38) :cw8 24 :ch8 38 ;; bottommid
                       :ex 43 :ey (- 128 20) :ew 24 :eh 20
                       :warp :map}
                      
                      :highroom2 {:name :highroom2
                                  :sprite :UpperRoomsInteriorHouse2
                                  :cx1 0 :cy1 0 :cw1 304 :ch1 48 ;; top
                                  :cx2 0 :cy2 0 :cw2 16 :ch2 240 ;; left
                                  :cx3 144 :cy3 0 :cw3 16 :ch3 240 ;; right
                                  :cx5 0 :cy5 (- 128 24) :cw5 42 :ch5 24 ;; bottomleft
                                  :cx6 42 :cy6 (- 128 16) :cw6 24 :ch6 16 ;; bottomdoor
                                  :cx7 (+ 42 24) :cy7 (- 128 24) :cw7 (- 304 42 24) :ch7 24 ;; bottomright                                  
                                  :ex 42 :ey (- 128 20) :ew 24 :eh 20
                                  :warp :map}
                                  
                      :highroom3 {:name :highroom3
                                  :sprite :UpperRoomsInteriorHouse3
                                  :cx1 0 :cy1 0 :cw1 304 :ch1 48 ;; top
                                  :cx2 0 :cy2 0 :cw2 16 :ch2 240 ;; left
                                  :cx3 144 :cy3 0 :cw3 16 :ch3 240 ;; right
                                  :cx5 0 :cy5 (- 128 24) :cw5 108 :ch5 24 ;; bottomleft
                                  :cx6 108 :cy6 (- 128 16) :cw6 24 :ch6 16 ;; bottomdoor
                                  :cx7 (+ 108 24) :cy7 (- 128 24) :cw7 (- 304 108 24) :ch7 24 ;; bottomright
                                  :ex 108 :ey (- 128 20) :ew 24 :eh 20                                  
                                  :warp :map} 
                      :highroom4 {:name :highroom4
                                  :sprite :UpperRoomsInteriorHouse4
                                   :cx1 0 :cy1 0 :cw1 304 :ch1 48 ;; top
                                   :cx2 0 :cy2 0 :cw2 16 :ch2 240 ;; left
                                   :cx3 288 :cy3 0 :cw3 16 :ch3 240 ;; right
                                   :cx4 140 :cy4 112 :cw4 24 :ch4 64 ;; mid
                                   :cx5 0 :cy5 (- 240 24) :cw5 108 :ch5 24 ;; bottomleft
                                   :cx6 108 :cy6 (- 240 16) :cw6 24 :ch6 16 ;; bottomdoor
                                   :cx7 (+ 108 24) :cy7 (- 240 24) :cw7 (- 304 108 24) :ch7 24 ;; bottomright
                                  :cx8 140 :cy8 (- 240 38) :cw8 24 :ch8 38 ;; bottommid
                                  :cx9 0 :cy9 (- 160 58) :cw9 256 :ch9 58 ;; topleft
                                  :cx10 (+ 256 16) :cy10 (- 160 58) :cw10 (- 304 256 16) :ch10 58 ;; topright
                                  :cx11 144 :cy11 0 :cw11 16 :ch11 128 ;; uppermid
                                  :ex 108 :ey (- 240 20) :ew 24 :eh 20
                                  :warp :map} 
                      :highroom5
                      {:name :highroom5
                       :sprite :UpperRoomsInteriorHouse5
                       :cx1 0 :cy1 0 :cw1 304 :ch1 48 ;; top
                       :cx2 0 :cy2 0 :cw2 16 :ch2 240 ;; left
                       :cx3 288 :cy3 0 :cw3 16 :ch3 240 ;; right
                       :cx4 140 :cy4 0 :cw4 24 :ch4 64 ;; mid
                       :cx5 0 :cy5 (- 128 24) :cw5 108 :ch5 24 ;; bottomleft
                       :cx6 108 :cy6 (- 128 16) :cw6 24 :ch6 16 ;; bottomdoor
                       :cx7 (+ 108 24) :cy7 (- 128 24) :cw7 (- 304 108 24) :ch7 24 ;; bottomright
                       :cx8 140 :cy8 (- 128 38) :cw8 24 :ch8 38 ;; bottommid
                       :ex 108 :ey (- 128 20) :ew 24 :eh 20
                       :warp :map}
                      :snow {:name :snow :sprite :AtlasSnow :h 800 :w 800}
                      :darkness {:name :darkness :w 800 :h 800 :alpha 1}
                      :innerDarkness {:name :darkness :w 800 :h 800 :alpha 0.95}
                      :chatbox {:name :chatbox :sprite :UIText :w 176 :h 128
                                :text :CHAT
                                :cx 3 :cy 86 :cw 29 :ch 40  :onclick :chatbox
                                :dx-max 160}
                      :menu {:name :menu :sprite :UIMenu :w 176 :h 128
                             :text :MENU
                             :cx 3 :cy 44 :cw 29 :ch 40 :onclick :menu
                             :dx-max 160}
                      :clue {:name :clue :sprite :UIClues :w 176 :h 128
                             :text :CLUE
                             :cx 3 :cy 2 :cw 29 :ch 40 :onclick :clue
                             :dx-max 160}
                      }
       
       )

(local object-brushes
       {:tree {:name :tree :sprite :AtlasTree
               :cx 14 :cy 28 :cw 4 :ch 4
               :w 32 :h 32
               :interaction :shake-nothing
               :ix 12 :iy 26 :iw 8 :ih 8}
        :carlington {:name :carlington :sprite :Carlington
                     :frames 2 :rate 0.4
                     :cx 6 :cy 12 :cw 4 :ch 4
                     :w 16 :h 16
                     :interaction :talk-carlington
                     :ix 0 :iy 8 :iw 16 :ih 16}
        :victoria {:name :victoria :sprite :Victoria
                     :frames 2 :rate 0.4
                     :cx 6 :cy 12 :cw 4 :ch 4
                     :w 16 :h 16
                     :interaction :talk-victoria
                   :ix 0 :iy 8 :iw 16 :ih 16}
        :william {:name :william :sprite :William
                     :frames 2 :rate 0.4
                     :cx 6 :cy 12 :cw 4 :ch 4
                     :w 16 :h 16
                     :interaction :talk-william
                  :ix 0 :iy 8 :iw 16 :ih 16}
        :alice {:name :alice :sprite :Alice
                     :frames 2 :rate 0.4
                     :cx 6 :cy 12 :cw 4 :ch 4
                     :w 16 :h 16
                     :interaction :talk-alice
                :ix 0 :iy 8 :iw 16 :ih 16}
        :martin {:name :martin :sprite :Martin
                     :frames 2 :rate 0.4
                     :cx 6 :cy 12 :cw 4 :ch 4
                     :w 16 :h 16
                     :interaction :talk-martin
                 :ix 0 :iy 8 :iw 16 :ih 16}
        :stanley {:name :stanley :sprite :Stanley
                     :frames 2 :rate 0.4
                     :cx 6 :cy 12 :cw 4 :ch 4
                     :w 16 :h 16
                     :interaction :talk-stanley
                  :ix 0 :iy 8 :iw 16 :ih 16}
        :jane {:name :jane :sprite :Jane
                     :frames 2 :rate 0.4
                     :cx 6 :cy 12 :cw 4 :ch 4
                     :w 16 :h 16
                     :interaction :talk-jane
               :ix 0 :iy 8 :iw 16 :ih 16}
        :samuel {:name :samuel :sprite :Samuel
                 :frames 2 :rate 0.4
                 :cx 6 :cy 12 :cw 4 :ch 4
                 :w 16 :h 16
                 :interaction :talk-samuel
                 :ix 0 :iy 8 :iw 16 :ih 16}
        :light {:name :light :sprite :AtlasLight
                :w 8 :h 8 :max-radius 1 :min-radius 0.7
                :frames 4 :rate 0.2}
        :smalllight {:name :smalllight :sprite :AtlasLightSmall
                     :w 8 :h 8 :max-radius 0.5 :min-radius 0.4
                     :frames 4 :rate 0.2}
        :fire {:name :fire :sprite :AtlasFire
               :w 16 :h 16 :max-radius 2 :min-radius 1.6
               :frames 4 :rate 0.5
               :cx 3 :cy 12 :cw 10 :ch 4}
        :firePlace {:name :firePlace :sprite :AtlasFirePlace
                    :w 32 :h 32 :max-radius 2 :min-radius 1.6
                    :frames 4 :rate 0.5
                    :cx 3 :cy 8 :cw 26 :ch 24}
        :counter {:name :counter :sprite :AtlasCounter :w 16 :h 32
                  :cx 0 :cy 8 :cw 12 :ch 24}
        :stanleyTable {:name :stanleyTable :sprite :AtlasPapers :w 16 :h 32
                     :cx 0 :cy 8 :cw 12 :ch 24
                     :interaction :paper-table-evidence-stanley
                     :ix -4 :iy 12 :iw 20 :ih 24}
        :samuelTable {:name :samuelTable :sprite :AtlasPapers :w 16 :h 32
                     :cx 0 :cy 8 :cw 12 :ch 24
                     :interaction :paper-table-evidence-samuel
                     :ix -4 :iy 12 :iw 20 :ih 24}        
        :doublebed {:name :doublebed :sprite :AtlasDoubleBed :w 32 :h 32
                    :cx 0 :cy 8 :cw 32 :ch 24}
        :bed {:name :bed :sprite :AtlasBed :w 16 :h 32
              :cx 0 :cy 8 :cw 16 :ch 24}
        :chest {:name :chest :sprite :AtlasChest :w 16 :h 16
                :cx 0 :cy 8 :cw 16 :ch 8
                :interaction :chest-evidence
                :ix -4 :iy 4 :iw 24 :ih 16}
        :chairfront {:name :chairfront :sprite :AtlasChairFront :w 16 :h 16
                     :cx 3 :cy 6 :cw 10 :ch 10}
        :chairside {:name :chairfront :sprite :AtlasChairSide :w 16 :h 16
                    :cx 6 :cy 6 :cw 10 :ch 10}        
        :curtain3 {:name :curtain3 :sprite :AtlasCurtain3 :w 48 :h 32}
        :curtain2 {:name :curtain2 :sprite :AtlasCurtain2 :w 32 :h 32}
        :curtain1 {:name :curtain1 :sprite :AtlasCurtain1 :w 32 :h 32}
        :knifeblock {:name :knifeblock :sprite :AtlasKnifeBlock :w 16 :h 32
                     :cx 0 :cy 8 :cw 12 :ch 24
                     :interaction :knife-block-evidence
                     :ix -4 :iy 12 :iw 20 :ih 24}
        :table {:name :table :sprite :AtlasTableLarge :w 32 :h 32
                     :cx 3 :cy 8 :cw 26 :ch 20
                     :interaction :table-evidence
                     :ix 0 :iy 0 :iw 32 :ih 32}
        :paperFire {:name :paperFire :sprite :AtlasLightSmall
                    :w 8 :h 8 :max-radius 0.7 :min-radius 0.5
                    :frames 4 :rate 0.5
                    :cx 1 :cy 2 :cw 6 :ch 6
                    :interaction :paper-fire-evidence
                    :ix -4 :iy -4 :iw 16 :ih 16
                    }
        :player {:name :player :sprite :AtlasPlayer
                 :cx 6 :cy 12 :cw 4 :ch 4
                 :w 16 :h 16}
        :house1 {:name :house1 :sprite :AtlasHouse1
                 :warp :room1
                 :w 96 :h 48
                 :cx 8 :cy 24 :cw 76 :ch 22
                 :ex 24 :ey 35 :ew 12 :eh 13
                 :sx 7 :sy 15
                 :px (+ 43 10) :py (- 128 20 10)}
       :house2 {:name :house2 :sprite :AtlasHouse2
                :warp :room2
                :w 64 :h 48
                :cx 7 :cy 24 :cw 44 :ch 22
                :ex 18 :ey 35 :ew 12 :eh 13
                :sx 7 :sy 15
                :px (+ 42 10) :py (- 128 20 10)}
       :house3 {:name :house3 :sprite :AtlasHouse3
                 :warp :room3
                 :w 64 :h 48
                 :cx 7 :cy 22 :cw 44 :ch 22
                 :ex 33 :ey 33 :ew 12 :eh 13
                :sx 7 :sy 13
                :px (+ 108 10) :py (- 128 20 10)}
       :house4 {:name :house4 :sprite :AtlasHouse4
                :warp :room4
                :w 144 :h 64
                :cx 11 :cy 38 :cw 122 :ch 22
                :ex 43 :ey 49 :ew 12 :eh 13
                :sx 11 :sy 27
                :px (+ 108 10) :py (- 240 20 10)}
        :house5 {:name :house5 :sprite :AtlasHouse5
                :warp :room5
                :w 112 :h 64
                :cx 9 :cy 39 :cw 94 :ch 22
                :ex 42 :ey 50 :ew 12 :eh 13
                 :sx 9 :sy 28
                 :px (+ 108 10) :py (- 128 20 10)}
        })

(local init false)
(tset state :col (bump.newWorld))
(var map 
     (level.load-map :assets.levels.map object-creation-callbacks object-preview-callbacks))
(set map.layers.objects.brushes object-brushes)
(when init
  (set map (level.create
            :map 514 514
            16
            atlas
            object-creation-callbacks
            object-preview-callbacks))
  (-> map 
      (map.add-layer :ground :objects ground-brushes)
      (map.add-layer :objects :objects object-brushes)
      (map.add-layer :cliff :objects cliff-brushes)
    (map.add-layer :ui :objects cliff-brushes)
               (map.add :objects 70 70 :player false)
               ;; (map.add :ground 0 0 :ground false)
               (map.add :ground 0 0 :road false)
               (map.add :ground 0 0 :footsteps false)
               (map.add :ground 0 0 :lowcliff false)
               (map.add :ground 0 0 :firesound false)
               ;; (map.add :objects 8 0 :house1 false)
               (map.add :cliff 0 0 :highcliff false)
               (map.add :cliff 0 0 :snow false)
               (map.add :cliff 0 0 :darkness false)
               (map.add :ui 0 0 :chatbox false)
               (map.add :ui 0 0 :menu false)
               (map.add :ui 0 0 :clue false)))
(set map.world state.col)


(tset state :col (bump.newWorld))
(local room1 (level.load-map :assets.levels.room1 object-creation-callbacks object-preview-callbacks))
(set room1.layers.objects.brushes object-brushes)
;; (local room1 (level.create
;;             :room1 304 128
;;             16
;;             atlas
;;             object-creation-callbacks
;;             object-preview-callbacks))
;; (-> room1
;;     (room1.add-layer :ground :objects ground-brushes)
;;     (room1.add-layer :objects :objects object-brushes)
;;     (room1.add-layer :cliff :objects cliff-brushes)
;;     (map.add-layer :ui :objects cliff-brushes)
;;     (map.add :objects 70 70 :player false)
;;     (room1.add :ground 0 0 :room1 false)
;;     (map.add :ground 0 0 :firesound false)
;;     (room1.add :cliff 0 0 :highroom1 false)
;;     (map.add :cliff 0 0 :innerDarkness false)
;;     (map.add :ui 0 0 :chatbox false)
;;     (map.add :ui 0 0 :menu false)
;;     (map.add :ui 0 0 :clue false)    
;;     )
(set room1.world state.col)


(tset state :col (bump.newWorld))
(local room2 (level.load-map :assets.levels.room2 object-creation-callbacks object-preview-callbacks))
(set room2.layers.objects.brushes object-brushes)
;; (local room2 (level.create
;;             :room2 160 128
;;             16
;;             atlas
;;             object-creation-callbacks
;;             object-preview-callbacks))
;; (-> room2
;;     (room2.add-layer :ground :objects ground-brushes)
;;     (room2.add-layer :objects :objects object-brushes)
;;     (room2.add-layer :cliff :objects cliff-brushes)
;;     (map.add-layer :ui :objects cliff-brushes)
;;     (map.add :objects 70 70 :player false)
;;     (room2.add :ground 0 0 :room2 false)
;;     (map.add :ground 0 0 :firesound false)
;;     (room2.add :cliff 0 0 :highroom2 false)
;;     (map.add :cliff 0 0 :innerDarkness false)
;;     (map.add :ui 0 0 :chatbox false)
;;     (map.add :ui 0 0 :menu false)
;;     (map.add :ui 0 0 :clue false)
;;     )
(set room2.world state.col)

(tset state :col (bump.newWorld))
(local room3 (level.load-map :assets.levels.room3 object-creation-callbacks object-preview-callbacks))
(set room3.layers.objects.brushes object-brushes)
;; (local room3 (level.create
;;             :room3 160 128
;;             16
;;             atlas
;;             object-creation-callbacks
;;             object-preview-callbacks))
;; (-> room3
;;     (room3.add-layer :ground :objects ground-brushes)
;;     (room3.add-layer :objects :objects object-brushes)
;;     (room3.add-layer :cliff :objects cliff-brushes)
;;     (map.add-layer :ui :objects cliff-brushes)
;;     (map.add :objects 70 70 :player false)
;;     (room3.add :ground 0 0 :room3 false)
;;     (map.add :ground 0 0 :firesound false)
;;     (room3.add :cliff 0 0 :highroom3 false)
;;     (map.add :cliff 0 0 :innerDarkness false)
;;     (map.add :ui 0 0 :chatbox false)
;;     (map.add :ui 0 0 :menu false)
;;     (map.add :ui 0 0 :clue false)    
;;     )
(set room3.world state.col)


(tset state :col (bump.newWorld))
(local room4 (level.load-map :assets.levels.room4 object-creation-callbacks object-preview-callbacks))
(set room4.layers.objects.brushes object-brushes)
;; (local room4 (level.create
;;             :room4 304 240
;;             16
;;             atlas
;;             object-creation-callbacks
;;             object-preview-callbacks))
;; (-> room4
;;     (room4.add-layer :ground :objects ground-brushes)
;;     (room4.add-layer :objects :objects object-brushes)
;;     (room4.add-layer :cliff :objects cliff-brushes)
;;     (map.add-layer :ui :objects cliff-brushes)
;;     (map.add :objects 70 70 :player false)
;;     (room4.add :ground 0 0 :room4 false)
;;     (map.add :ground 0 0 :firesound false)
;;     (room4.add :cliff 0 0 :highroom4 false)
;;     (map.add :cliff 0 0 :darkness false)
;;     (map.add :ui 0 0 :chatbox false)
;;     (map.add :ui 0 0 :menu false)
;;     (map.add :ui 0 0 :clue false)    
;;     )
(set room4.world state.col)

(tset state :col (bump.newWorld))
(local room5 (level.load-map :assets.levels.room5 object-creation-callbacks object-preview-callbacks))
(set room5.layers.objects.brushes object-brushes)
;; (local room5 (level.create
;;             :room5 304 128
;;             16
;;             atlas
;;             object-creation-callbacks
;;             object-preview-callbacks))
;; (-> room5
;;     (room5.add-layer :ground :objects ground-brushes)
;;     (room5.add-layer :objects :objects object-brushes)
;;     (room5.add-layer :cliff :objects cliff-brushes)
;;     (map.add-layer :ui :objects cliff-brushes)
;;     (map.add :objects 70 70 :player false)
;;     (room5.add :ground 0 0 :room5 false)
;;     (map.add :ground 0 0 :firesound false)
;;     (room5.add :cliff 0 0 :highroom5 false)
;;     (map.add :cliff 0 0 :innerDarkness false)
;;     (map.add :ui 0 0 :chatbox false)
;;     (map.add :ui 0 0 :menu false)
;;     (map.add :ui 0 0 :clue false)    
;;     )
(set room5.world state.col)

(set state.col map.world)
;; (rebrush map :bgobjects)
(editor.init map {:x 0 :y 0 :scale 6})
;;(set state.player (. map.layers.objects.data 1))
(set state.level map)
  (each [_ obj (ipairs map.layers.objects.data)]
    (when (= :player obj.type)
      (set state.player obj))
    )
(set state.player.footstep-volume 0.4)
(set state.target.snap true)

(fn set-level [self name px py]
  (local l (. self name))
  (set state.col l.world)
  (set state.level l)
  (editor.init l {:x 0 :y 0 :scale 6})
  (each [_ obj (ipairs l.layers.objects.data)]
    (when (= :player obj.type)
      (if obj.last-xy
          (obj:translate obj.last-xy.x obj.last-xy.y)
          (obj:translate px py)
          )
      (set state.target.snap true)
      (set state.player obj))
    )
  (match name
    :map (set state.player.footstep-volume 0.4))
  ;; (set state.player (. l.layers.objects.data 1))
  l)

(fn save [self]
  (each [name level (pairs self)]
    (level:save (.. "assets/levels/" name ".fnl"))))

(setmetatable {: map : room1 : room2
                : room3 : room4 : room5
               } {:__index {: set-level : save}})
