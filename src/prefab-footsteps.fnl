(local footsteps {})

(fn footsteps.serialize [self] self.brush)

(fn add-step [self x y angle]
  (tset self.prints self.index {: x : y : angle :foot self.foot})
  (if (= self.foot :left)
      (tset self :foot :right)
      (tset self :foot :left))
  (tset self :index (+ self.index 1))
  (when (> self.index self.max)
    (tset self :index 1))
  (self.spritebatch:clear)
  (let [quad {:left self.quad-left :right self.quad-right}]
    (each [_ {: x : y : angle : foot} (ipairs self.prints)]
      (self.spritebatch:add (. quad foot) x y (+ angle (/ math.pi 2)) 1 1 4 4)))
  (self.spritebatch:flush))

(fn footsteps.update [self dt]
  (local player (. (require :state) :player))

  (local {:col {: x : y} : angle} player)

  (let [distance (lume.distance self.last-footprint.x self.last-footprint.y x y)]
    (when (> distance self.stride)
        (do (add-step self x y angle) ;; this could be done more accuratly
            (tset self :last-footprint {: x : y})))))

(fn footsteps.draw [self]
  (when self.spritebatch
    (love.graphics.push "all")
    (love.graphics.setColor 1 1 1 1)
    (love.graphics.draw self.spritebatch 0 0)
    (love.graphics.pop)))

(local footsteps-mt {:__index footsteps
                     :update footsteps.update
                     :draw footsteps.draw})

(fn footsteps.preview [])

(fn footsteps.create [brush]
  (local {: x : y : sprite-left : sprite-right : stride : max} brush)
  (local {:quad quad-left : image} (. resources.atlas sprite-left))
  (local {:quad quad-right} (. resources.atlas sprite-right))
  (let [maxp (or max 100)
        spritebatch  (love.graphics.newSpriteBatch image (+ max 1) :static)]
    (setmetatable {:last-footprint {:x x :y y}
                   : x
                   : y
                   : brush
                   :w 100
                   :h 100
                   :prints []
                   : quad-left
                   : quad-right
                   : spritebatch
                   : stride
                   :max maxp
                   :foot :left
                   :index 1}
                  footsteps-mt)))

footsteps
