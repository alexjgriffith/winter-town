(local sounds {})
(local object-base (require :object))
(setmetatable sounds {:__index object-base})

(fn sounds.draw [self])

(fn sounds.interact [self player])

(fn y-offset [y who]
  (+ y (match who
         :light 4
         :smalllight 4
         :fire 8
         :player 8
         :firePlace 16
         _ 0)))

(fn x-offset [x who]
  (+ x (match who
         :light 4
         :smalllight 4
         :fire 8
         :player 8
         :firePlace 16
         _ 0)))

(fn sounds.update [self dt]
  (local data (. (require :state) :level :layers :objects :data))
  (local player (. (require :state) :player))
  (local {: x : y} player)
  (local t self.target)
  (local sounds (lume.filter data (fn [d] (= t d.name))))
  (local roll-off self.roll-off)
  (var d nil)
  (each [_ s (ipairs sounds)]
    (local d1 (lume.distance
               (x-offset x :player)
               (y-offset y :player)
               (x-offset s.x t)
               (y-offset s.y t)))
    (when (not d) (set d d1))
    (when (< d1 d) (set d d1)))
  (local max+roll-off (+ roll-off self.proximity))
  (when d
    (if (< d self.proximity) (self.sound:setVolume self.max-volume)
      ;; (< d max+roll-off)
      (self.sound:setVolume (* self.max-volume
                               (/ (- roll-off (- d self.proximity))
                                  roll-off)))
      ;; (self.sound:setVolume 0)
      ))
  )

(fn sounds.leave [self dt]
  (self.sound:stop))

(fn sounds.enter [self dt]
  (self.sound:setVolume 0)
  (self.sound:setLooping true)
  (self.sound:play 0))

(local sounds-mt {:__index sounds})

(fn sounds.preview [brush])

(fn sounds.create [brush]
  (local {: name : sound-name : proximity : target : max-volume
          : roll-off} brush)
  (local sound (. resources sound-name))
  (local obj
         (setmetatable
          {:brush brush
           : sound
           :w 100
           :h 100
           :x 0
           :y 0
           : proximity
           : roll-off
           : target
           : max-volume
           :type :sounds
           } sounds-mt))
  obj)

sounds
