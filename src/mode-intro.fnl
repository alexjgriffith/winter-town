(local menu {})

(local gamestate (require :lib.gamestate))

(local fade (require :lib.fade))
(var click-down false)
(var hovered false)
(local lg love.graphics)
(var hover-reset true)

(fn click [text dx]
  (local down (and (= 0 dx) (love.mouse.isDown 1)))
  (if down
      (when (not click-down)
        (resources.click:stop)
        (resources.click:play)
        (match text
          :Back (fade.out (fn []                             
                            (gamestate.switch (require :mode-game))))
          )
        (set click-down true)
        )
      (do
        (when (and (~= text hovered) (= 0 dx))
          (do (set hovered text)
              (resources.hover:stop)
              (resources.hover:play)))
        (set click-down false))))


(fn draw-small-button [text mx my x y image upquad downquad iconquad dx]
  (local (bw bh) (values 10 10))
  (lg.setColor 1 0 0 1)
  (lg.setColor 1 1 1 1)
  (if (pointWithin mx my (+ x 2) (+ y 2) bw bh)
      (do (lg.draw image upquad x y)
          (click text dx)
          (set hover-reset false)
          (lg.draw image iconquad (+ x 4) (+ y 4)))
      (do (lg.draw image downquad x y)
          (lg.draw image iconquad (+ x 4) (+ y 3)))))

(fn menu.enter [self]
  (fade.in))

(local intro-text "The Marquise has been murdered! It is up to you to track down the culprit.

It could be the neglected wife, the stilted lover, the debter, or anyone else in our cast of possible villains!

His wife, the Marchioness Victoria, will be very intersted to hear your conclusion.")

(fn menu.draw []
  (local dx 0)
  (local (px py) (love.mouse.getPosition))
  (local (mx my) (values (/ px 5) (/ py 5)))
  (local {:quad blankQuad} (. resources.atlas :UIBlank))
  (local {:quad downSmallQuad} (. resources.atlas :UIButtonSmallUp))
  (local {:quad upSmallQuad} (. resources.atlas :UIButtonSmallDown))
  (local {: image :quad xQuad} (. resources.atlas :UIX))
  (set hover-reset true)
  (lg.push)
  (lg.scale 5)
  (lg.setColor resources.black)
  (lg.rectangle :fill 0 0 200 200)
  (lg.setColor 1 1 1 1)
  (lg.draw image blankQuad)
  (lg.setColor resources.black)
  (lg.setFont resources.text-font)
  (lg.printf intro-text 34 9 110)
  (lg.setColor 1 1 1 1)
  (draw-small-button :Back mx my (+ 48 dx 64 16) 106 image upSmallQuad downSmallQuad xQuad dx)
  (lg.draw fade.canvas)
  (lg.pop)
  (when hover-reset
    (set hovered false))
  )

(fn menu.keypressed [self key code]
  (match code
    :return (do
             (resources.click:stop)
             (resources.click:play)
             (fade.out (fn []                             
                         (gamestate.switch (require :mode-game))))
             )
    :escape (do
             (resources.click:stop)
             (resources.click:play)
             (fade.out (fn []                             
                         (gamestate.switch (require :mode-game))))
             )    
    :space (do
             (resources.click:stop)
             (resources.click:play)
             (fade.out (fn []                             
                         (gamestate.switch (require :mode-game))))
          )
             ))


menu
