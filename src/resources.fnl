(local music {
              :bgm :winter-town.mp3
              })
(local sfx {:fire :fire.ogg
            :click :Click.ogg
            :hover :Hover.ogg
            :talk :Talk.ogg
            :footsteps :footsteps.ogg
            })
(local ap (require :lib.aseprite-parser))
(local {: hex-to-rgba} (require :lib.colour))

(local resources {})

(fn resources.initialize []
  (each [key value (pairs music)]
    (local new-music
           (love.audio.newSource (.. "assets/music/" value)
                                 (if web
                                     "static"
                                     "stream")))
    (new-music:setLooping true)
    (new-music:setVolume 0.15)
    (tset resources key new-music))
  
  (each [key value (pairs sfx)]
    (local new-sfx
           (love.audio.newSource (.. "assets/sounds/" value)
                                 :static))
    (new-sfx:setVolume 0.025)
    (tset resources key new-sfx))
  (tset resources :atlas (ap.slices-as-quads "assets/sprites/map.json"))
  (tset resources :debug-font (love.graphics.newFont "assets/fonts/inconsolata.otf" 10))
  (tset resources :button-font (love.graphics.newFont "assets/fonts/FreePixel.ttf" 16))
  (tset resources :text-font (love.graphics.newFont "assets/fonts/pixel.tff" 8))
  (tset resources :title-font (love.graphics.newFont "assets/fonts/pixel.tff" 16))
  (tset resources :white (hex-to-rgba "#faf8f0"))
  (tset resources :grey (hex-to-rgba "#d98e88"))
  (tset resources :brown (hex-to-rgba "#652b2b"))
  (tset resources  :black (hex-to-rgba "#1a1010"))
  (tset resources  :cursor (love.mouse.newCursor "assets/sprites/CleanCursor-export.png"))
  (resources.talk:setLooping true)
  (resources.talk:setVolume 0)
  (resources.talk:play 0)
  )

resources
