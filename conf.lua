love.conf = function(t)
   t.title, t.identity = "murder-of-the-marquise", "murder-of-the-marquise"
   t.modules.joystick = false
   t.modules.physics = false
   -- (insert (pp (cons (* 8 16 5) (* 11 16 5))))
   t.window.width = 880 --  5 (11 x 16)(640 . 960)
   t.window.height = 640 --  5 (8 x 16)
   t.window.vsync = true
   t.version = "11.3"
   t.window.resizable=false
end
